package com.example.alissongaliza.bartender.Model.Database;

import android.content.ContentValues;

public class Gerencia_Cartao {

    public static final String TAB_CARTOES = "cartoes";
    public static final String NUMERO_CARTOES = "numeCarto";
    public static final String NOME_CARTOES = "nomeCarto";
    public static final String VALIDADE_CARTOES = "valiCarto";
    public static final String CVV_CARTOES = "cvvCarto";
    public static final String CPF_CARTOES = "cpfCarto";
    public static final String DEBITO_CARTOES = "debiCarto";
    public static final String CREDITO_CARTOES = "credCarto";


    public static String criaComentario() {

        String SQL = "CREATE TABLE IF NOT EXISTS" + OpenHelper.NOME_BANCO + "." + TAB_CARTOES + "(" +
                NUMERO_CARTOES + " CHAR(16) NOT NULL," +
                NOME_CARTOES + "VARCHAR(45) NOT NULL," +
                VALIDADE_CARTOES + "DATE NOT NULL COMMENT," +
                CVV_CARTOES + "VARCHAR(4) NOT NULL," +
                CPF_CARTOES + "CHAR(11) NOT NULL," +
                DEBITO_CARTOES + "TINYINT(1) NULL," +
                CREDITO_CARTOES + "TINYINT(1) NULL," +
                "PRIMARY KEY (" + NUMERO_CARTOES + "))" +
                "ENGINE = InnoDB";
        return SQL;
    }

    protected ContentValues gerarContentValuesCartoes(POJO_Cartao cartoes) {

        ContentValues values = new ContentValues();
        values.put(NUMERO_CARTOES, cartoes.getId());
        values.put(NOME_CARTOES, cartoes.getNomeCarto());
        values.put(VALIDADE_CARTOES, cartoes.getValiCarto());
        values.put(CVV_CARTOES, cartoes.getCvvCarto());
        values.put(CPF_CARTOES, cartoes.getCpfCarto());
        values.put(DEBITO_CARTOES, cartoes.isDebiCarto());
        values.put(CREDITO_CARTOES, cartoes.isCredCarto());

        return values;
    }
}
