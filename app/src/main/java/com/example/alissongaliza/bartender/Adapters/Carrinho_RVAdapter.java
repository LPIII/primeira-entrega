package com.example.alissongaliza.bartender.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.R;

import java.util.ArrayList;

/**
 * Created by Alisson on 26/10/2017.
 */

public class Carrinho_RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final String TAG = "Carrinho_RVAdapter";
    private ArrayList<Bebida> bebidas;

    public interface CarrinhoRV_ClickListener {
        void onClick(View view, int position);
    }

    private CarrinhoRV_ClickListener mListener;

    public Carrinho_RVAdapter(CarrinhoRV_ClickListener listener) {
        mListener = listener;
        bebidas = new ArrayList<>();
    }

    private class ProdutosViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView foto;
        TextView nome;
        TextView estiloPais;
        TextView quantidade;
        TextView precoTotalNum;
        ImageButton removeUm;
        ImageButton adicionaUm;

        CarrinhoRV_ClickListener mListener;

        ProdutosViewHolder(View itemView, CarrinhoRV_ClickListener listener) {
            super(itemView);

            foto = itemView.findViewById(R.id.carrinho_pedidos_imageView_fotoProduto);
            nome = itemView.findViewById(R.id.carrinho_pedidos_textView_nomeProduto);
            estiloPais = itemView.findViewById(R.id.carrinho_pedidos_textView_informacoes);
            quantidade = itemView.findViewById(R.id.carrinho_pedidos_quantidade);
            precoTotalNum = itemView.findViewById(R.id.carrinho_pedidos_precoTotal);
            removeUm = itemView.findViewById(R.id.carrinho_pedidos_imageButton_removeUm);
            adicionaUm = itemView.findViewById(R.id.carrinho_pedidos_imageButton_adicionaUm);

            mListener = listener;
            adicionaUm.setOnClickListener(this);
            removeUm.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            mListener.onClick(view, getAdapterPosition());
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.carrinho_pedidos, parent, false);
        return new ProdutosViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: starts ");

        ProdutosViewHolder vh = (ProdutosViewHolder) holder;

        vh.nome.setText(bebidas.get(position).getNome());
        vh.estiloPais.setText(bebidas.get(position).getEstilo_pais());
        vh.quantidade.setText(String.valueOf(bebidas.get(position).getQuantidadePedida()));
        vh.precoTotalNum.setText(String.valueOf(bebidas.get(position).getPreco() * bebidas.get(position).getQuantidadePedida()));


    }

    @Override
    public int getItemCount() {
        return bebidas.size();
    }

    public void atualizaPedido(ArrayList<Bebida> bebidas){
        this.bebidas.clear();
        this.bebidas.addAll(bebidas);
        notifyDataSetChanged();
    }
}
