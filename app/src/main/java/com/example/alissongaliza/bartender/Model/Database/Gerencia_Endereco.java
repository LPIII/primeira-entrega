package com.example.alissongaliza.bartender.Model.Database;


import android.content.ContentValues;
import android.provider.BaseColumns;

public  class Gerencia_Endereco implements BaseColumns {
    public static final String TAB_ENDERECO = "endereco";
    public static final String IDENDER_ENDERECO = "idEnder";
    public static final String CLIENTE_CPFCLIEN_ENDERECO = "cliente_cpfClien";
    public static final String UF_PAIS_CODPAIS_ENDERECO = "uf_pais_codPais";
    public static final String UF_CODUF_ENDERECO = "uf_codUf";
    public static final String CIDAENDER_ENDERECO = "cidaEnder";
    public static final String RUAENDER_ENDERECO = "ruaEnder";
    public static final String BAIRENDER_ENDERECO = "bairEnder";
    public static final String CEPENDER_ENDERECO = "cepEnder";
    public static final String NUMEENDER_ENDERECO = "numeEnder";
    public static final String COMPENDER_ENDERECO = "compEnder";



    public String criaEndereco() {

        String SQL = "CREATE TABLE IF NOT EXISTS"+OpenHelper.NOME_BANCO+"."+TAB_ENDERECO+"("+
                IDENDER_ENDERECO+ "BIGINT NOT NULL AUTO_INCREMENT," +
                CLIENTE_CPFCLIEN_ENDERECO+ "CHAR(11) NOT NULL," +
                UF_PAIS_CODPAIS_ENDERECO+ "CHAR(3) NOT NULL," +
                UF_CODUF_ENDERECO+ "CHAR(2) NOT NULL," +
                CIDAENDER_ENDERECO+ "VARCHAR(30) NOT NULL," +
                RUAENDER_ENDERECO+ "VARCHAR(60) NOT NULL ," +
                BAIRENDER_ENDERECO+ "VARCHAR(30) NOT NULL," +
                CEPENDER_ENDERECO+ "CHAR(8) NOT NULL," +
                NUMEENDER_ENDERECO+ "VARCHAR(6) NOT NULL," +
                COMPENDER_ENDERECO+ "VARCHAR(100) NOT NULL," +
                "PRIMARY KEY ("+IDENDER_ENDERECO+"),"+
                "UNIQUE INDEX idEnder_UNIQUE ("+IDENDER_ENDERECO+" ASC),"+
                "INDEX fk_endereco_cliente1_idx ("+CLIENTE_CPFCLIEN_ENDERECO+" ASC),"+
                "CONSTRAINT fk_endereco_cliente1"+
                "FOREIGN KEY ("+CLIENTE_CPFCLIEN_ENDERECO+")"+
                "REFERENCES "+OpenHelper.NOME_BANCO+"."+TAB_ENDERECO+"("+Gerencia_Usuario.CPFUSUAR_USUARIO+")"+
                "ON DELETE NO ACTION"+
                "ON UPDATE NO ACTION)";

        return SQL;
    }

    protected ContentValues gerarContentValuesEndereco (POJO_Endereco endereco) {
        ContentValues values = new ContentValues();
        values.put(IDENDER_ENDERECO, endereco.getId());
        values.put(CLIENTE_CPFCLIEN_ENDERECO, endereco.getCliente_cpfClien());
        values.put(UF_PAIS_CODPAIS_ENDERECO, endereco.getUf_pais_codPais());
        values.put(UF_CODUF_ENDERECO, endereco.getUf_codUf());
        values.put(CIDAENDER_ENDERECO, endereco.getCidaEnder());
        values.put(RUAENDER_ENDERECO, endereco.getRuaEnder());
        values.put(BAIRENDER_ENDERECO, endereco.getBairEnder());
        values.put(CEPENDER_ENDERECO, endereco.getCepEnder());
        values.put(NUMEENDER_ENDERECO, endereco.getNumeEnder());
        values.put(COMPENDER_ENDERECO, endereco.getCompEnder());

        return values;
    }
}