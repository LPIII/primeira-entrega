package com.example.alissongaliza.bartender.Presenter;

import com.example.alissongaliza.bartender.Interfaces.PerfilContract;

/**
 * Created by Alisson on 11/04/18.
 */

public class PerfilPresenter implements PerfilContract.Presenter {
    
    private PerfilContract.View mView;

    public PerfilPresenter(PerfilContract.View view) {
        this.mView = view;
    }

    @Override
    public void subscribe() {
        //TODO:RxJava
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void meusDadosClicked() {

    }

    @Override
    public void meusEnderecosClicked() {

    }

    @Override
    public void bartenderAmigoClicked() {

    }

    @Override
    public void pagamentoClicked() {

    }

    @Override
    public void avaliarClicked() {

    }

    @Override
    public void faqClicked() {

    }

    @Override
    public void fotoPerfilClicked() {

    }
}
