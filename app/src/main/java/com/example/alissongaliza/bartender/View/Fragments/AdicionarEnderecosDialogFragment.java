package com.example.alissongaliza.bartender.View.Fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alissongaliza.bartender.Interfaces.OnAddAddressResultsAvailable;
import com.example.alissongaliza.bartender.JavaClasses.ComunicaComServer;
import com.example.alissongaliza.bartender.R;

import java.util.HashMap;

/**
 * Created by Alisson on 06/02/2018.
 */

public class AdicionarEnderecosDialogFragment extends DialogFragment implements View.OnClickListener,
                                                                                OnAddAddressResultsAvailable {

    public interface AoAdicionarEndereco{
        void AdicionarEndereco(DialogFragment dialog);
    }



    AoAdicionarEndereco mListener;
    Button addEndereco;
    EditText cep;
    EditText endereco;
    EditText numero;
    EditText complemento;
    EditText bairro;
    EditText cidade;


//    public static AdicionarEnderecosDialogFragment newInstance(AoAdicionarEndereco listener) {
//        AdicionarEnderecosDialogFragment f = new AdicionarEnderecosDialogFragment();
//        f.mListener = listener;
//
//        return f;
//    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.70f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.cadastrar_endereco, null);

        addEndereco = view.findViewById(R.id.cadastrar_endereco_editText_cadastrar);
        cep = view.findViewById(R.id.cadastrar_endereco_editText_cep);
        endereco = view.findViewById(R.id.cadastrar_endereco_editText_endereco);
        numero = view.findViewById(R.id.cadastrar_endereco_editText_numero);
        complemento = view.findViewById(R.id.cadastrar_endereco_editText_complemento);
        bairro = view.findViewById(R.id.cadastrar_endereco_editText_bairro);
        cidade = view.findViewById(R.id.cadastrar_endereco_editText_cidade);


        addEndereco.setOnClickListener(this);
        builder.setView(view);

        return builder.create();
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//
//        try {
//
//            mListener = (AoAdicionarEndereco) context;
//
//        } catch (ClassCastException e) {
//
//            throw new ClassCastException(context.toString()
//                    + " must implement AoAdicionarEndereco");
//        }
//    }

    @Override
    public void onClick(View view) {
        HashMap<String, Object> map = new HashMap<>();
        ComunicaComServer server = new ComunicaComServer(this, getActivity());
        server.execute(constroiMap(map));
    }

    @Override
    public void onAddAddressResultsAvailable(HashMap<String, Object> data) {
        Resources res = getResources();
        if(data.get(res.getString(R.string.ENTRADA_STATUS)).equals(res.getString(R.string.ENTRADA_STATUS_OK))){
            //faz algo com o id retornado no map
            Toast.makeText(getActivity(), "Endereço adicionado", Toast.LENGTH_SHORT).show();
            dismiss();
        }
    }

    private HashMap<String, Object> constroiMap(HashMap<String, Object> map){
        Resources res = getResources();
        map.put(res.getString(R.string.ENTRADA_ACAO),res.getString(R.string.ACAO_CADASTRAR_ENDERECO));
        map.put(res.getString(R.string.ENTRADA_CIDADE),cidade.getText().toString());
        map.put(res.getString(R.string.ENTRADA_RUA),endereco.getText().toString());
        map.put(res.getString(R.string.ENTRADA_BAIRRO),bairro.getText().toString());
        map.put(res.getString(R.string.ENTRADA_CEP),cep.getText().toString());
        map.put(res.getString(R.string.ENTRADA_NUMERO),numero.getText().toString());
        map.put(res.getString(R.string.ENTRADA_COMPLEMENTO),complemento.getText().toString());
        map.put(res.getString(R.string.ENTRADA_UF_NOME),"paraiba");
        map.put(res.getString(R.string.ENTRADA_PAIS_NOME),"brasil");
        map.put(res.getString(R.string.ENTRADA_CLIENTE_CPF),"70120600154");
        map.put(res.getString(R.string.KEY_CREDENCIAL_LOGIN), "admin");
        map.put(res.getString(R.string.KEY_CREDENCIAL_SENHA), "123");

        return map;
    }

}
