package com.example.alissongaliza.bartender.View.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.alissongaliza.bartender.Presenter.HomePresenter;
import com.example.alissongaliza.bartender.View.Activities.CarrinhoViewActivity;
import com.example.alissongaliza.bartender.View.Activities.ProdutoDetalheViewActivity;
import com.example.alissongaliza.bartender.View.Fragments.CategoriasViewFragment;
import com.example.alissongaliza.bartender.View.Fragments.HomeViewFragment;
import com.example.alissongaliza.bartender.View.Fragments.PedidosViewFragment;
import com.example.alissongaliza.bartender.View.Fragments.PerfilViewFragment;
import com.example.alissongaliza.bartender.Helpers.BottomNavigationViewHelper;
import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainViewActivity extends AppCompatActivity {

    private static final String TAG = "MainViewActivity";
    HomeViewFragment homeViewFragment;
    CategoriasViewFragment categoriasViewFragment;
    PerfilViewFragment perfilViewFragment;
    PedidosViewFragment pedidosViewFragment;
//    MenuItem itemAtual;
    FloatingActionButton fab;
    TextView nomeMarca;
    ArrayList<Bebida> bebidas;
    EditText ip;
    EditText porta;
    Button mudar;
    public static String ipTexto;
    public static String portaTexto;

    Intent oldIntent;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            bebidas = (ArrayList<Bebida>) intent.getSerializableExtra(ARRAYLIST_DE_BEBIDAS);
        }
    };


    public static final String ARRAYLIST_DE_BEBIDAS = "Lista de bebidas a serem mostradas no carrinho";
    private static final String PREFS_TAG = "Tag para identificar o sharedPreference que eu to usando - usado para lista de bebidas";
    private static final String PRODUCT_TAG = "Tag para recuperar a lista de bebidas no sharedPreferences";
    public static final String ATUALIZAR_LISTA_DE_BEBIDAS = "Atualizar Lista De Bebidas";


    private HomePresenter presenter;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Log.d(TAG, "onNavigationItemSelected: starts");

//            if(item.getItemId() != itemAtual.getItemId()){
//                itemAtual = item;
                switch (item.getItemId()) {
                    case R.id.navigation_home:
//                        getSupportFragmentManager().
//                                beginTransaction().
//                                replace(R.id.content, homeViewFragment).
//                                commit();
                        return true;
                    case R.id.navigation_pedidos:

                        if(pedidosViewFragment == null){
                           pedidosViewFragment = new PedidosViewFragment();
                        }
//                        getSupportFragmentManager().
//                                beginTransaction().
//                                replace(R.id.content, pedidosViewFragment).
//                                commit();
                        return true;
                    case R.id.navigation_categorias:

                        if(categoriasViewFragment == null){
                            categoriasViewFragment = new CategoriasViewFragment();
                        }
//                        getSupportFragmentManager().
//                                beginTransaction().
//                                replace(R.id.content, categoriasViewFragment).
//                                commit();
                        return true;
                    case R.id.navigation_perfil:

                        if(perfilViewFragment == null){
                            perfilViewFragment = new PerfilViewFragment();
                        }
//                        getSupportFragmentManager().
//                                beginTransaction().
//                                replace(R.id.content, perfilViewFragment).
//                                commit();
                        return true;
                }
//            }
            return false;
        }

    };

    public interface AdicionarAoCarrinho{
        void adicionaAoCarrinho(ArrayList<Bebida> bebidas);
    }

    public MainViewActivity() {
        bebidas = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.disableShiftMode(navigation);

        fab = findViewById(R.id.floatingActionButton);
        fab.setVisibility(View.INVISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CarrinhoViewActivity.class);
                intent.putExtra(ARRAYLIST_DE_BEBIDAS, bebidas);
                startActivity(intent);
            }
        });

        homeViewFragment = new HomeViewFragment();
        showHomeOnStart();



        nomeMarca = findViewById(R.id.main_textView_brand);
        ip = findViewById(R.id.main_ip_teste);
        porta = findViewById(R.id.main_porta_teste);
        mudar = findViewById(R.id.main_mudar_teste);


        ipTexto = ip.getText().toString();
        portaTexto = porta.getText().toString();

        //chamar isso para mudar a fonte
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/beyond_the_mountains.ttf");
        nomeMarca.setTypeface(font);
        nomeMarca.setTextSize(26);

        mudar.setOnClickListener(view -> {
            ipTexto = ip.getText().toString();
            portaTexto = porta.getText().toString();
            ip.setText(ip.getText());
            porta.setText(porta.getText());
        });




//        Intent intent = new Intent(this,PagamentoViewActivity.class);
//        startActivity(intent);
//
//        Intent intent1 = new Intent(this,CreateAccountViewActivity.class);
//        startActivity(intent1);


    }

    private void showHomeOnStart(){
        //exibe tela inicial (home)

        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.content, homeViewFragment).
                commit();
        presenter = new HomePresenter(homeViewFragment);

    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(ATUALIZAR_LISTA_DE_BEBIDAS));

        Intent intent = getIntent();
        String className = intent.getStringExtra("Class");
        if(intent != oldIntent) {

            if (className != null && className.equals(ProdutoDetalheViewActivity.CLASS_NAME)) {
                //significa que tem uma bebida a ser adicionada ao carrinho

                Bebida bebidaParaCarrinho = (Bebida) intent.getSerializableExtra(ProdutoDetalheViewActivity.OBJ_BEBIDA);
                bebidas = recuperaListaDeBebidas();
                bebidas.add(bebidaParaCarrinho);

                oldIntent = intent; // TODO: Analisar se esse oldIntent ainda faz sentido quando estivermos voltando de várias activities


            } else if (className != null && className.equals(CarrinhoViewActivity.CLASS_NAME)) {
                //vindo do carrinho (até agora essa transição só acontece apertando o botao de voltar no aparelho). Apenas atualiza
                //a lista de bebidas caso alguma quantidade tenha sido alterada na tela de carrinho
                bebidas = recuperaListaDeBebidas();

                oldIntent = intent;
            }
        }

        if(!bebidas.isEmpty()){
            fab.setVisibility(View.VISIBLE);
        }




    }

    @Override
    protected void onPause() {
        super.onPause();

        salvaListaDeBebidas(bebidas);
    }

    @Override
    protected void onDestroy() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver); //TODO: Testar mais se eu devo dar unregister aqui mesmo
        super.onDestroy();
    }

    /**
     * Salva lista de bebidas atual em Json para recuperar quando voltar para a atividade
     * @param b Lista de bebidas a ser salva
     */
    private void salvaListaDeBebidas(List<Bebida> b){

        Gson gson = new Gson();
        String jsonCurProduct = gson.toJson(b);

        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(PREFS_TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString(PRODUCT_TAG, jsonCurProduct);
        editor.apply();
    }

    /**
     * A função recupera o Json com a lista de bebidas a serem adicionadas ao carrinho
     * @return ArrayList de bebidas
     */
    private ArrayList<Bebida> recuperaListaDeBebidas(){

        Gson gson = new Gson();
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(PREFS_TAG, Context.MODE_PRIVATE);
        String jsonPreferences = sharedPref.getString(PRODUCT_TAG, "");
        Type type = new TypeToken<List<Bebida>>() {}.getType();

        return gson.fromJson(jsonPreferences, type);
    }

}
