package com.example.alissongaliza.bartender.Model.Database;

/**
 *
 * @author Rhenan Castelo Branco
 */
public class POJO_Cartao implements Persistencia{
    
    private String numeCarto;
    private String nomeCarto ;
    private String valiCarto;
    private String cvvCarto;
    private String cpfCarto;
    private boolean debiCarto;
    private boolean credCarto;

    public POJO_Cartao(){


    }

    public POJO_Cartao(String numeCarto, String nomeCarto, String valiCarto, String cvvCarto, String cpfCarto, boolean debiCarto, boolean credCarto) {
        super();
        this.numeCarto = numeCarto;
        this.nomeCarto = nomeCarto;
        this.valiCarto = valiCarto;
        this.cvvCarto = cvvCarto;
        this.cpfCarto = cpfCarto;
        this.debiCarto = debiCarto;
        this.credCarto = credCarto;
    }


    @Override
    public String getId() {
        return numeCarto;
    }

    @Override
    public void setId(String numeCarto) {
        this.numeCarto = numeCarto;
    }

    /*public String getNumeCarto() {
        return numeCarto;
    }

    public void setNumeCarto(String numeCarto) {
        this.numeCarto = numeCarto;
    }*/

    public String getNomeCarto() {
        return nomeCarto;
    }

    public void setNomeCarto(String nomeCarto) {
        this.nomeCarto = nomeCarto;
    }

    public String getValiCarto() {
        return valiCarto;
    }

    public void setValiCarto(String valiCarto) {
        this.valiCarto = valiCarto;
    }

    public String getCvvCarto() {
        return cvvCarto;
    }

    public void setCvvCarto(String cvvCarto) {
        this.cvvCarto = cvvCarto;
    }

    public String getCpfCarto() {
        return cpfCarto;
    }

    public void setCpfCarto(String cpfCarto) {
        this.cpfCarto = cpfCarto;
    }

    public boolean isDebiCarto() {
        return debiCarto;
    }

    public void setDebiCarto(boolean debiCarto) {
        this.debiCarto = debiCarto;
    }

    public boolean isCredCarto() {
        return credCarto;
    }

    public void setCredCarto(boolean credCarto) {
        this.credCarto = credCarto;
    }


}
