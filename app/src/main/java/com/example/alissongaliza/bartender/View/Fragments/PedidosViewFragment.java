package com.example.alissongaliza.bartender.View.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alissongaliza.bartender.Adapters.Pedidos_categorias_RVAdapter;
import com.example.alissongaliza.bartender.Enum.Status;
import com.example.alissongaliza.bartender.Interfaces.PedidosContract;
import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.JavaClasses.Pedido;
import com.example.alissongaliza.bartender.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PedidosViewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PedidosViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PedidosViewFragment extends Fragment implements PedidosContract.View {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    RecyclerView categoriasRV;

    PedidosContract.Presenter presenter;

    public PedidosViewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PedidosViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PedidosViewFragment newInstance(String param1, String param2) {
        PedidosViewFragment fragment = new PedidosViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_pedidos, container, false);

        categoriasRV = view.findViewById(R.id.pedidos_RV);

        ArrayList<Pedido> listaDePedidos = new ArrayList<>();   //temporario para teste
        criaPedidos(listaDePedidos);
        categoriasRV.setAdapter(new Pedidos_categorias_RVAdapter(listaDePedidos,getContext()));
        categoriasRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        return view;
    }

    private void criaPedidos(ArrayList<Pedido> lista){
        ArrayList<Bebida> bebidas = new ArrayList<>();
        Bebida b1 = new Bebida("Dolly","Seu amiguinho","Refrigerante",1.50f,
                ".","~Recife~","Brasil","Preto","0%",
                "1 a 10ºC");
        b1.setQuantidadePedida(3);
        bebidas.add(b1);
        Pedido p1 = new Pedido(Status.ABERTO,"19382092139234234234312392","Avenida dos escoteiros, 190 - Mangabeira","15/05/2018",
                                "18:56",bebidas,"Cartão de crédito Master", "0896",
                (float)4.50,(float)10.0,(float)0.0,(float)14.50,846846);
        lista.add(p1);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
        presenter.subscribe();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        presenter.unsubscribe();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void setPresenter(PedidosContract.Presenter presenterAbstraction) {
        this.presenter = presenterAbstraction;
    }
}
