package com.example.alissongaliza.bartender.Presenter;

import com.example.alissongaliza.bartender.Interfaces.PedidosContract;

/**
 * Created by luiz on 11/04/18.
 */

public class PedidosPresenter implements PedidosContract.Presenter {

    PedidosContract.View mView;

    public PedidosPresenter(PedidosContract.View mView){
        this.mView = mView;
        mView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void orderDetailsClicked(String idPedido) {

    }
}
