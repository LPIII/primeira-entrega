package com.example.alissongaliza.bartender.Presenter;

import com.example.alissongaliza.bartender.Interfaces.ProdutoPorCategoriaContract;

/**
 * Created by Alisson on 11/04/18.
 */

public class ProdutoPorCategoriaPresenter implements ProdutoPorCategoriaContract.Presenter{

    ProdutoPorCategoriaContract.View view;

    public ProdutoPorCategoriaPresenter(ProdutoPorCategoriaContract.View view) {
        this.view = view;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {
        //RxJava
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void bebidaDetalhes(String idBebida) {

    }
}
