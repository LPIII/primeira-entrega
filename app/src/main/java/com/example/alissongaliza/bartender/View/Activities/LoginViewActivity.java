package com.example.alissongaliza.bartender.View.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.alissongaliza.bartender.Interfaces.LoginContract;
import com.example.alissongaliza.bartender.R;

public class LoginViewActivity extends AppCompatActivity implements LoginContract.View{

    Button fazerLogin;
    Button fazerLoginPeloGoogle;
    Button fazerLoginPeloFacebook;
    Button criarConta;
    EditText campoUsuario;
    EditText campoSenha;
    TextView cadastre;

    public static final String NOME_USUARIO = "Nome do usuario";

    private LoginContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        fazerLogin = findViewById(R.id.login_button_fazerLogin);
        fazerLoginPeloFacebook = findViewById(R.id.login_button_facebook);
        fazerLoginPeloGoogle = findViewById(R.id.login_button_google);
        criarConta = findViewById(R.id.login_button_criarConta);
        campoUsuario = findViewById(R.id.login_editText_usuario);
        campoSenha = findViewById(R.id.login_editText_senha);
        cadastre = findViewById(R.id.login_textView_cadastre);

        Intent intent = getIntent();
        campoUsuario.setText(intent.getStringExtra(NOME_USUARIO));

        criarConta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CreateAccountViewActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void setPresenter(LoginContract.Presenter presenterAbstraction) {
        this.presenter = presenterAbstraction;
    }

    @Override
    public void credenciaisErradas(String message) {

    }

    @Override
    public void conexaoExternaFalhou(String message) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unsubscribe();
    }
}
