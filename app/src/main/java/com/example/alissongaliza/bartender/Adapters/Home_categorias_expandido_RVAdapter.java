package com.example.alissongaliza.bartender.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alissongaliza.bartender.View.Activities.ProdutoDetalheViewActivity;
import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.R;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Alisson on 04/10/2017.
 */

public class Home_categorias_expandido_RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "Home_horizontal_recycle";
    ArrayList<Bebida> listaDeBebidas;
    private Context mContext;
    public static final String OBJ_BEBIDA = "ListaDeBebidas";
    public static final String BEBIDA_IMAGEM = "BebidaImagem";

    public Home_categorias_expandido_RVAdapter(Context context, ArrayList<Bebida> bebidas) {
        mContext = context;
        listaDeBebidas = bebidas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: starts ");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_categorias_expandido, parent, false);
        return new BebidaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: starts ");

        Resources resources = mContext.getResources();
        BebidaViewHolder vh = (BebidaViewHolder) holder;
        vh.nome.setText(listaDeBebidas.get(position).getNome());

        //temporario até termos imagens no banco de dados da aplicação
        switch(listaDeBebidas.get(position).getNome()){
            case "Fanta":
                vh.bebida.setImageResource(R.mipmap.ic_fanta);
                break;
            case "Coca-Cola":
                vh.bebida.setImageResource(R.mipmap.ic_coca);
                break;
            case "Pepsi Cola":
                vh.bebida.setImageResource(R.mipmap.ic_pepsi);
                break;
            case "Dolly":
                vh.bebida.setImageResource(R.mipmap.ic_dolly);
                break;
            case "Ciroc":
                vh.bebida.setImageResource(R.mipmap.ic_ciroc);
                break;
            case "Absolut":
                vh.bebida.setImageResource(R.mipmap.ic_absolut);
                break;
            case "Skol lata":
                vh.bebida.setImageResource(R.mipmap.ic_skol);
                break;
            case "Heineken":
                vh.bebida.setImageResource(R.mipmap.ic_heineken);
                break;


        }
        vh.preco.setText(String.format(Locale.ENGLISH,resources.getString(R.string.home_expandido_preco), listaDeBebidas.get(position).getPreco()));



    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: starts ");
        return listaDeBebidas.size();
    }


    private class BebidaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private static final String TAG = "BebidaViewHolder";
        ImageView bebida = null;
        TextView preco = null;
        TextView nome = null;

        public BebidaViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "BebidaViewHolder: starts ");

            bebida = itemView.findViewById(R.id.home_expandido_imageView_fotoBebida);
            preco = itemView.findViewById(R.id.home_expandido_textView_preco);
            nome = itemView.findViewById(R.id.home_expandido_textView_nomeBebida);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), ProdutoDetalheViewActivity.class);
            intent.putExtra(ProdutoDetalheViewActivity.OBJ_BEBIDA, listaDeBebidas.get(getAdapterPosition()));
            view.getContext().startActivity(intent);
        }

    }

}
