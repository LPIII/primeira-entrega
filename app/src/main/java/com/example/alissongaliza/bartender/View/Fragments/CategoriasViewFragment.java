package com.example.alissongaliza.bartender.View.Fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.alissongaliza.bartender.Interfaces.CategoriasContract;
import com.example.alissongaliza.bartender.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CategoriasViewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CategoriasViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoriasViewFragment extends Fragment implements CategoriasContract.View{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    Button cerveja;
    Button vinho;
    Button whiskey;

    private OnFragmentInteractionListener mListener;

    CategoriasContract.Presenter presenter;

    public CategoriasViewFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_categorias, container, false);

        cerveja = view.findViewById(R.id.categorias_button_cerveja);
        vinho = view.findViewById(R.id.categorias_button_vinho);
        whiskey = view.findViewById(R.id.categorias_button_whiskey);

        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/beyond_the_mountains.ttf");
        cerveja.setTypeface(font);
        vinho.setTypeface(font);
        whiskey.setTypeface(font);




        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
        presenter.subscribe();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        presenter.unsubscribe();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void setPresenter(CategoriasContract.Presenter presenterAbstraction) {
        this.presenter = presenterAbstraction;
    }
}
