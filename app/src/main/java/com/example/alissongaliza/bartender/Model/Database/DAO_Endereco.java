package com.example.alissongaliza.bartender.Model.Database;

import android.content.ContentValues;
import android.content.Context;

public class DAO_Endereco extends DAO<POJO_Endereco> {

    public DAO_Endereco(Context context) {
        super(context);
    }

    @Override
    public String getNomeColunaPrimaryKey() {
        return Gerencia_Endereco.IDENDER_ENDERECO;
    }

    @Override
    public String getNomeTabela() {
        return Gerencia_Endereco.TAB_ENDERECO;
    }

    @Override
    public ContentValues entidadeParacontentValues(POJO_Endereco entidade) {
        ContentValues values = new ContentValues();

        if (!entidade.getId().equals("0")) {

            values.put(Gerencia_Endereco.IDENDER_ENDERECO, entidade.getId());

        }
        values.put(Gerencia_Endereco.CLIENTE_CPFCLIEN_ENDERECO, entidade.getCliente_cpfClien());
        values.put(Gerencia_Endereco.UF_PAIS_CODPAIS_ENDERECO, entidade.getUf_pais_codPais());
        values.put(Gerencia_Endereco.UF_CODUF_ENDERECO, entidade.getUf_codUf());
        values.put(Gerencia_Endereco.CIDAENDER_ENDERECO, entidade.getCidaEnder());
        values.put(Gerencia_Endereco.RUAENDER_ENDERECO, entidade.getRuaEnder());
        values.put(Gerencia_Endereco.BAIRENDER_ENDERECO, entidade.getBairEnder());
        values.put(Gerencia_Endereco.CEPENDER_ENDERECO, entidade.getCepEnder());
        values.put(Gerencia_Endereco.NUMEENDER_ENDERECO, entidade.getNumeEnder());
        values.put(Gerencia_Endereco.COMPENDER_ENDERECO, entidade.getCompEnder());
        return values;
    }

    @Override
    public POJO_Endereco contentValuesParaEntidade(ContentValues contentValues) {




        return null;
    }
}
