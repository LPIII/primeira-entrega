package com.example.alissongaliza.bartender.Presenter;

import com.example.alissongaliza.bartender.Interfaces.ProdutoDetalheContract;
import com.example.alissongaliza.bartender.JavaClasses.Bebida;

/**
 * Created by ci on 11/04/18.
 */

public class ProdutoDetalhePresenter implements ProdutoDetalheContract.Presenter {

    ProdutoDetalheContract.View view;

    public ProdutoDetalhePresenter(ProdutoDetalheContract.View view) {
        this.view = view;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {
        //RxJava
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void adicionarAoCarrinho(Bebida bebida) {
        view.respostaAdicionarAoCarrinho("OK");
    }

    @Override
    public void menosUmClicked(Bebida bebida) {
        bebida.setQuantidadePedida(bebida.getQuantidadePedida() - 1 );
        view.atualizarView(bebida);
    }

    @Override
    public void maisUmClicked(Bebida bebida) {
        bebida.setQuantidadePedida(bebida.getQuantidadePedida() + 1 );
        view.atualizarView(bebida);
    }
}
