package com.example.alissongaliza.bartender.View.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alissongaliza.bartender.Adapters.ProdutoPorCategoria_RVAdapter;
import com.example.alissongaliza.bartender.Interfaces.ProdutoPorCategoriaContract;
import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.R;

import java.util.ArrayList;

import static com.example.alissongaliza.bartender.View.Activities.ProdutosPorCategoriaViewActivity.OBJ_BEBIDA;


public class ProdutoPorCategoriaViewFragment extends Fragment implements ProdutoPorCategoriaContract.View{
    private static final String TAG = "ProdutoPorCategoriaFrag";


    private ProdutoPorCategoriaContract.Presenter presenter;

    private RecyclerView RV;
    private ArrayList<Bebida> bebidasPorCategoria;
    public ProdutoPorCategoriaViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ProdutoPorCategoriaViewFragment newInstance(ArrayList<Bebida> bebidas){
        ProdutoPorCategoriaViewFragment fragment = new ProdutoPorCategoriaViewFragment();
        Bundle args = new Bundle();
        args.putSerializable(OBJ_BEBIDA, bebidas);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_produto_por_categoria_view, container, false);
        if(getArguments() != null){
            bebidasPorCategoria = (ArrayList<Bebida>) getArguments().getSerializable(OBJ_BEBIDA);
        }

        RV = view.findViewById(R.id.produtoCategoria_RV);
        RV.setAdapter(new ProdutoPorCategoria_RVAdapter(getContext(), bebidasPorCategoria));
        RV.setLayoutManager(new GridLayoutManager(getContext(), 3));

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



    @Override
    public void setPresenter(ProdutoPorCategoriaContract.Presenter presenterAbstraction) {
        this.presenter = presenterAbstraction;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.unsubscribe();
    }
}
