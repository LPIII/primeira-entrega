package com.example.alissongaliza.bartender.View.Activities;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alissongaliza.bartender.Adapters.ProdutoDetalhe_RVAdapter;
import com.example.alissongaliza.bartender.Interfaces.OnAddCarrinhoResultsAvailable;
import com.example.alissongaliza.bartender.Interfaces.ProdutoDetalheContract;
import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.JavaClasses.ComunicaComServer;
import com.example.alissongaliza.bartender.Presenter.ProdutoDetalhePresenter;
import com.example.alissongaliza.bartender.Presenter.ProdutoPorCategoriaPresenter;
import com.example.alissongaliza.bartender.R;
import com.example.alissongaliza.bartender.View.Fragments.ProdutoDetalheViewFragment;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class ProdutoDetalheViewActivity extends AppCompatActivity{
    private static final String TAG = "ProdutoDetalheViewActiv";

    Bebida bebida;
    HashMap<String,Object> map;

    public static final String OBJ_BEBIDA = "Obj bebida";
    public static final String CLASS_NAME = "ProdutoDetalheViewActivity";

    ProdutoDetalhePresenter presenter;

    public ProdutoDetalheViewActivity() {
        map = new HashMap<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produto_detalhe);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        bebida = (Bebida) intent.getSerializableExtra(OBJ_BEBIDA);

        ProdutoDetalheViewFragment fragment =
                ( ProdutoDetalheViewFragment ) getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment == null) {
            fragment = ProdutoDetalheViewFragment.newInstance(bebida);
        }
        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.content, fragment).
                commit();
        presenter = new ProdutoDetalhePresenter(fragment);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return true;
    }


}
