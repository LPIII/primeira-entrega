package com.example.alissongaliza.bartender.Model.Database;
import java.util.List;

/**
 *
 * @author Rhenan Castelo Branco
 * @autor Gabriel Belarmino
 */
public class POJO_Usuario implements Persistencia {
    
    private String cpfUsuar; 
    private String nomeUsuar; 
    private String sobrUsuar; 
    private String teleUsuar; 
    private String imagUsuar; 
    private String emaiUsuar; 
    private String logiUsuar; 
    private String senhUsuar; 
    private String nascUsuar; 
    private String criaUsuar; 
    private String atuaUsuar; 
    private List <POJO_Endereco> endeUsuar;
    private List <POJO_Cartao> cartUsuar;

    public POJO_Usuario(String cpfUsuar, String nomeUsuar, String sobrUsuar,
                        String teleUsuar, String imagUsuar, String emaiUsuar,
                        String logiUsuar, String senhUsuar, String nascUsuar,
                        String criaUsuar, String atuaUsuar, List <POJO_Endereco> endeUsuar) {

        super();
        this.cpfUsuar = cpfUsuar;
        this.nomeUsuar = nomeUsuar;
        this.sobrUsuar = sobrUsuar;
        this.teleUsuar = teleUsuar;
        this.imagUsuar = imagUsuar;
        this.emaiUsuar = emaiUsuar;
        this.logiUsuar = logiUsuar;
        this.senhUsuar = senhUsuar;
        this.nascUsuar = nascUsuar;
        this.criaUsuar = criaUsuar;
        this.atuaUsuar = atuaUsuar;
        this.endeUsuar = endeUsuar; 
    }
    
    public POJO_Usuario(String cpfUsuar, String nomeUsuar, String sobrUsuar,
                        String teleUsuar, String imagUsuar, String emaiUsuar,
                        String logiUsuar, String senhUsuar, String nascUsuar,
                        String criaUsuar, String atuaUsuar, List <POJO_Endereco> endeUsuar, List<POJO_Cartao> cartUsuar) {
        
        this.cpfUsuar = cpfUsuar;
        this.nomeUsuar = nomeUsuar;
        this.sobrUsuar = sobrUsuar;
        this.teleUsuar = teleUsuar;
        this.imagUsuar = imagUsuar;
        this.emaiUsuar = emaiUsuar;
        this.logiUsuar = logiUsuar;
        this.senhUsuar = senhUsuar;
        this.nascUsuar = nascUsuar;
        this.criaUsuar = criaUsuar;
        this.atuaUsuar = atuaUsuar;
        this.endeUsuar = endeUsuar; 
        this.cartUsuar = cartUsuar; 
    }
    
    public POJO_Usuario(String cpfUsuar, String nomeUsuar, String sobrUsuar,
                        String teleUsuar, String imagUsuar, String emaiUsuar,
                        String logiUsuar, String senhUsuar, String nascUsuar,
                        String criaUsuar, String atuaUsuar) {

        this.cpfUsuar = cpfUsuar;
        this.nomeUsuar = nomeUsuar;
        this.sobrUsuar = sobrUsuar;
        this.teleUsuar = teleUsuar;
        this.imagUsuar = imagUsuar;
        this.emaiUsuar = emaiUsuar;
        this.logiUsuar = logiUsuar;
        this.senhUsuar = senhUsuar;
        this.nascUsuar = nascUsuar;
        this.criaUsuar = criaUsuar;
        this.atuaUsuar = atuaUsuar;
    }
    
    public POJO_Usuario() {}
    
    public POJO_Usuario(String logiUsuar, String senhUsuar) {
    this.logiUsuar = logiUsuar; 
    this.senhUsuar = senhUsuar; 
    }

    /*public String getCpfUsuar() {
        return cpfUsuar;
    }

    public void setCpfUsuar(String cpfUsuar) {
        this.cpfUsuar = cpfUsuar;
    }
    */
    public String getNomeUsuar() {
        return nomeUsuar;
    }

    public void setNomeUsuar(String nomeUsuar) {
        this.nomeUsuar = nomeUsuar;
    }

    public String getSobrUsuar() {
        return sobrUsuar;
    }

    public void setSobrUsuar(String sobrUsuar) {
        this.sobrUsuar = sobrUsuar;
    }

    public String getTeleUsuar() {
        return teleUsuar;
    }

    public void setTeleUsuar(String teleUsuar) {
        this.teleUsuar = teleUsuar;
    }

    public String getImagUsuar() {
        return imagUsuar;
    }

    public void setImagUsuar(String imagUsuar) {
        this.imagUsuar = imagUsuar;
    }

    public String getEmaiUsuar() {
        return emaiUsuar;
    }

    public void setEmaiUsuar(String emaiUsuar) {
        this.emaiUsuar = emaiUsuar;
    }

    public String getLogiUsuar() {
        return logiUsuar;
    }

    public void setLogiUsuar(String logiUsuar) {
        this.logiUsuar = logiUsuar;
    }

    public String getSenhUsuar() {
        return senhUsuar;
    }

    public void setSenhUsuar(String senhUsuar) {
        this.senhUsuar = senhUsuar;
    }

    public String getNascUsuar() {
        return nascUsuar;
    }

    public void setNascUsuar(String nascUsuar) {
        this.nascUsuar = nascUsuar;
    }

    public String getCriaUsuar() {
        return criaUsuar;
    }

    public void setCriaUsuar(String criaUsuar) {
        this.criaUsuar = criaUsuar;
    }

    public String getAtuaUsuar() {
        return atuaUsuar;
    }

    public void setAtuaUsuar(String atuaUsuar) {
        this.atuaUsuar = atuaUsuar;
    }

    @Override
    public String getId() {
        return cpfUsuar;
    }

    @Override
    public void setId(String cpfUsuar) {
    this.cpfUsuar = cpfUsuar;
    }
}

