package com.example.alissongaliza.bartender.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.R;

import java.util.ArrayList;

/**
 * Created by Alisson on 15/01/2018.
 */

public class Pedidos_detalhe_categorias_expandido_RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<Bebida> mBebidas;

    public Pedidos_detalhe_categorias_expandido_RVAdapter(Context context, ArrayList<Bebida> bebidas) {
        mContext = context;
        mBebidas = bebidas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pedidos_detalhe_listaprodutos_expandido, parent, false);
        return new Bebidas(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Bebidas vh = (Bebidas) holder;

        vh.estiloPais.setText(mBebidas.get(position).getEstilo_pais());
        vh.nome.setText(mBebidas.get(position).getNome());
        vh.quantidade.setText(String.valueOf(mBebidas.get(position).getQuantidadePedida()));
        vh.total.setText("???");

    }

    @Override
    public int getItemCount() {
        return mBebidas.size();
    }

    private class Bebidas extends RecyclerView.ViewHolder{

        ImageView imagem;
        TextView nome;
        TextView estiloPais;
        TextView quantidade;
        TextView total;

        Bebidas(View itemView) {
            super(itemView);
            imagem = itemView.findViewById(R.id.pedidos_detalhe_listaprodutos_expandido_imageView_produto);
            nome = itemView.findViewById(R.id.pedidos_detalhe_listaprodutos_expandido_textView_nome);
            estiloPais = itemView.findViewById(R.id.pedidos_detalhe_listaprodutos_expandido_textView_estiloPais);
            quantidade = itemView.findViewById(R.id.pedidos_detalhe_listaprodutos_expandido_textView_quantidade);
            total = itemView.findViewById(R.id.pedidos_detalhe_listaprodutos_expandido_textView_total);
        }
    }
}
