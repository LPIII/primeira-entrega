package com.example.alissongaliza.bartender.Presenter;


import com.example.alissongaliza.bartender.Interfaces.HomeContract;

/**
 * Created by luiz on 11/04/18.
 */

public class HomePresenter implements HomeContract.Presenter{

    HomeContract.View mView;

    public HomePresenter(HomeContract.View mView) {
        this.mView = mView;
        this.mView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        //RxJava
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void showProductsByCategory(String categoria) {

    }

    @Override
    public void showProductDetail(String idProduct) {

    }
}
