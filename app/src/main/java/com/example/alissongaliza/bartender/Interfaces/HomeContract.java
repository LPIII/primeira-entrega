package com.example.alissongaliza.bartender.Interfaces;

import com.example.alissongaliza.bartender.JavaClasses.Bebida;

import java.util.ArrayList;

/**
 * Created by Alisson & Luiz on 10/04/18.
 */

public interface HomeContract {
    interface View extends BaseView<Presenter> {
        
    }
    
    interface Presenter extends BasePresenter {
        void showProductsByCategory(String categoria);
        void showProductDetail(String idProduct);
        
    }
}
