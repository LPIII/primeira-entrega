package com.example.alissongaliza.bartender.Model.Database;

public class POJO_Comentario implements Persistencia {

    private int idComen;
    private String produto_codiProdu;
    private String nomeComen;
    private String dataComen;
    private String avalComen;
    private String comeComen;
    private String imagComen;

    public POJO_Comentario() {

    }

    public POJO_Comentario(int idComen, String produto_codiProdu, String nomeComen, String dataComen, String avalComen, String comeComen) {
        super();
        this.idComen = idComen;
        this.produto_codiProdu = produto_codiProdu;
        this.nomeComen = nomeComen;
        this.dataComen = dataComen;
        this.avalComen = avalComen;
        this.comeComen = comeComen;
    }

    public String getComeComen() {
        return comeComen;
    }

    public void setComeComen(String comeComen) {
        this.comeComen = comeComen;
    }

    /*public int getIdComent() {
        return idComent;
    }

    public void setIdComent(int idComent) {
        this.idComen = idComent;
    }
    */
    public String getProduto_codiProdu() {
        return produto_codiProdu;
    }

    public void setProduto_codiProdu(String produto_codiProdu) {
        this.produto_codiProdu = produto_codiProdu;
    }

    public String getnomeClien() {
        return nomeComen;
    }

    public void setnomeClien(String nomeComent) {
        this.nomeComen = nomeComent;
    }

    public String getDataComen() {
        return dataComen;
    }

    public void setDataComen(String dataComen) {
        this.dataComen = dataComen;
    }

    public String getAvalComen() {
        return avalComen;
    }

    public void setAvalComen(String avalComen) {
        this.avalComen = avalComen;
    }

    public String getImagComen() {
        return imagComen;
    }

    public void setImagComen(String imagComen) {
        this.imagComen = imagComen;
    }

    @Override
    public String getId() {
        return Integer.toString(idComen);
    }

    @Override
    public void setId(String idComen) {
    this.idComen = Integer.parseInt(idComen);

    }
}


