package com.example.alissongaliza.bartender.Interfaces;

/**
 * Created by Alisson on 08/04/2018.
 */

public interface BaseView<T> {
    void setPresenter(T presenterAbstraction);
}
