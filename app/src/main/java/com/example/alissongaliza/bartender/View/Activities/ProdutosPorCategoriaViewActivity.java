package com.example.alissongaliza.bartender.View.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.Presenter.ProdutoPorCategoriaPresenter;
import com.example.alissongaliza.bartender.R;
import com.example.alissongaliza.bartender.View.Fragments.ProdutoPorCategoriaViewFragment;

import java.util.ArrayList;

public class ProdutosPorCategoriaViewActivity extends AppCompatActivity{

    private static final String TAG = "ProdutosPorCategoriaVie";

    ProdutoPorCategoriaPresenter presenter;
    private ArrayList<Bebida> bebidasPorCategoria;
    public static final String OBJ_BEBIDA = "BEBIDAS";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produtos_por_categoria);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        bebidasPorCategoria = (ArrayList<Bebida>) args.getSerializable(OBJ_BEBIDA);

        getSupportActionBar().setTitle(bebidasPorCategoria.get(0).getCategoria());



        ProdutoPorCategoriaViewFragment fragment =
                (ProdutoPorCategoriaViewFragment) getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment == null) {
            fragment = ProdutoPorCategoriaViewFragment.newInstance(bebidasPorCategoria);
        }
        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.content, fragment).
                commit();
        presenter = new ProdutoPorCategoriaPresenter(fragment);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return true;
    }

}
