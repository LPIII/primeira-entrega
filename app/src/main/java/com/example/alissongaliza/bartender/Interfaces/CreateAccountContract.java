package com.example.alissongaliza.bartender.Interfaces;

import java.util.HashMap;

/**
 * Created by Alisson on 08/04/2018.
 */

public interface CreateAccountContract {
    interface View extends BaseView<Presenter>{
        void onEmailError(String message);
        void showConfirmationMessage();
    }

    interface Presenter extends BasePresenter{
        void createAccountClicked(HashMap<String, Object> params);
    }
}
