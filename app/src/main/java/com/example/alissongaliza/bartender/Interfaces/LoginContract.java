package com.example.alissongaliza.bartender.Interfaces;

import java.util.HashMap;

/**
 * Created by Alisson on 11/04/18.
 */

public interface LoginContract {
    interface View extends BaseView<Presenter>{
        void credenciaisErradas(String message);
        void conexaoExternaFalhou(String message);
    }

    interface Presenter extends BasePresenter{
        void fazerLogin(HashMap<String, Object> params);
        void criarConta(HashMap<String, Object> params);
        void entrarGoogle();
        void entrarFacebook();


    }
}
