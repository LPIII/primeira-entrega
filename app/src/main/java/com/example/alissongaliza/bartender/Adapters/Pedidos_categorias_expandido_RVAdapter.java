package com.example.alissongaliza.bartender.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alissongaliza.bartender.View.Activities.PedidoDetalheViewActivity;
import com.example.alissongaliza.bartender.JavaClasses.Pedido;
import com.example.alissongaliza.bartender.R;

import java.util.ArrayList;

/**
 * Created by Alisson on 11/01/2018.
 */

public class Pedidos_categorias_expandido_RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private ArrayList<Pedido> listaDePedidosPorStatus;
    private Context mContext;


    public Pedidos_categorias_expandido_RVAdapter(Context context, ArrayList<Pedido> listaDePedidosPorStatus) {
        this.mContext = context;
        this.listaDePedidosPorStatus = listaDePedidosPorStatus;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (listaDePedidosPorStatus.size() > 0) {
            View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pedidos_expandido, parent, false);
            return new Pedidos(view1);
        }
        else {
            View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pedidos_expandido_vazio, parent, false);
            return new Pedidos(view2);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Pedidos vh = (Pedidos) holder;
        Resources res = mContext.getResources();
        if(listaDePedidosPorStatus.size() > 0) {

            vh.dataHora.setText(res.getString(R.string.pedidos_expandido_textView_dataHora,
                                    listaDePedidosPorStatus.get(position).getData(),
                                    listaDePedidosPorStatus.get(position).getHora()));
            vh.numPedido.setText(res.getString(R.string.pedidos_expandido_textView_numPedido,
                                    String.valueOf(listaDePedidosPorStatus.get(position).getNumPedido())));
            vh.preco.setText(res.getString(R.string.pedidos_expandido_textView_preco,
                                listaDePedidosPorStatus.get(position).getTotalComDesconto()));
        }
        else{
            vh.instrucao.setText(res.getString(R.string.pedidos_expandido_nenhumPedido));
        }
    }

    @Override
    public int getItemCount() {
        if(listaDePedidosPorStatus.size() > 0){
            return listaDePedidosPorStatus.size();
        }
        else{
            return 1;       //mostrando as instrucoes
        }

    }

    private class Pedidos extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView status;
        TextView dataHora;
        TextView numPedido;
        TextView preco;
        TextView instrucao;

        Pedidos(View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.pedidos_expandido_imageView_status);
            dataHora = itemView.findViewById(R.id.pedidos_expandido_textView_dataHora);
            numPedido = itemView.findViewById(R.id.pedidos_expandido_textView_numPedido);
            preco = itemView.findViewById(R.id.pedidos_expandido_textView_preco);
            instrucao = itemView.findViewById(R.id.pedidos_expandido_vazio_instrucao);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), PedidoDetalheViewActivity.class);
            intent.putExtra(PedidoDetalheViewActivity.OBJ_PEDIDO, listaDePedidosPorStatus.get(getAdapterPosition()));
            view.getContext().startActivity(intent);
        }
    }
}
