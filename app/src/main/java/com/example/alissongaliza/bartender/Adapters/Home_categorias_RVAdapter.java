package com.example.alissongaliza.bartender.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.View.Activities.ProdutosPorCategoriaViewActivity;
import com.example.alissongaliza.bartender.R;

import java.util.ArrayList;

/**
 * Created by Alisson on 04/10/2017.
 */

public class Home_categorias_RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "Home_vertical_recyclerV";

    private Context mContext;
    private ArrayList<Bebida> listaDeBebidasGeral;
    //uma lista para cada tipo de bebida
    private ArrayList<Bebida> listaDeRefrigerantes;
    private ArrayList<Bebida> listaDeVodkas;
    private ArrayList<Bebida> listaDeCervejas;



    public Home_categorias_RVAdapter(Context context, ArrayList<Bebida> listaDeBebidas) {
        Log.d(TAG, "Home_categorias_RVAdapter: starts ");

        mContext = context;
        this.listaDeBebidasGeral = listaDeBebidas;
        listaDeCervejas = new ArrayList<>();
        listaDeVodkas = new ArrayList<>();
        listaDeRefrigerantes = new ArrayList<>();
        separarBebidas(listaDeBebidasGeral);
    }

    private class ListaCervejaViewHolder extends RecyclerView.ViewHolder{
        RecyclerView listaBebidasRV;
        Button mais;
        TextView nomeCategoria;


        ListaCervejaViewHolder(View itemView) {
            super(itemView);

            listaBebidasRV = (RecyclerView) itemView.findViewById(R.id.home_horizontal_recyclerView);
            nomeCategoria = (TextView) itemView.findViewById(R.id.home_textView_nomeCategoria);
            mais = (Button) itemView.findViewById(R.id.home_button_mais);
        }
    }

    private class ListaRefrigeranteViewHolder extends RecyclerView.ViewHolder{
        RecyclerView listaBebidasRV;
        Button mais;
        TextView nomeCategoria;


        ListaRefrigeranteViewHolder(View itemView) {
            super(itemView);

            listaBebidasRV = (RecyclerView) itemView.findViewById(R.id.home_horizontal_recyclerView);
            nomeCategoria = (TextView) itemView.findViewById(R.id.home_textView_nomeCategoria);
            mais = (Button) itemView.findViewById(R.id.home_button_mais);
        }
    }

    private class ListaVodkaViewHolder extends RecyclerView.ViewHolder{
        RecyclerView listaBebidasRV;
        Button mais;
        TextView nomeCategoria;


        ListaVodkaViewHolder(View itemView) {
            super(itemView);

            listaBebidasRV = (RecyclerView) itemView.findViewById(R.id.home_horizontal_recyclerView);
            nomeCategoria = (TextView) itemView.findViewById(R.id.home_textView_nomeCategoria);
            mais = (Button) itemView.findViewById(R.id.home_button_mais);
        }
    }

    @Override
    public int getItemViewType(int position) {
        Log.d(TAG, "getItemViewType: starts");
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: starts ");

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_categorias, parent, false);
        switch (viewType){
            case 0:
                return new ListaCervejaViewHolder(view);

            case 1:
                return new ListaRefrigeranteViewHolder(view);

            case 2:
                return new ListaVodkaViewHolder(view);

            default:
                return new ListaCervejaViewHolder(view);
        }



    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: starts ");

        switch (holder.getItemViewType()){
            case 0:
                ListaCervejaViewHolder vhCerveja = (ListaCervejaViewHolder) holder;
                vhCerveja.nomeCategoria.setText(listaDeCervejas.get(0).getCategoria());
                vhCerveja.mais.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext,ProdutosPorCategoriaViewActivity.class);
                        Bundle args = new Bundle();
                        args.putSerializable(ProdutosPorCategoriaViewActivity.OBJ_BEBIDA, listaDeCervejas);
                        intent.putExtra("BUNDLE",args);
                        mContext.startActivity(intent);
                    }
                });
                vhCerveja.listaBebidasRV.setAdapter(new Home_categorias_expandido_RVAdapter(mContext, listaDeCervejas));
                vhCerveja.listaBebidasRV.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false));

                break;

            case 1:
                ListaRefrigeranteViewHolder vhRefrigerante = (ListaRefrigeranteViewHolder) holder;
                vhRefrigerante.nomeCategoria.setText(listaDeRefrigerantes.get(0).getCategoria());
                vhRefrigerante.mais.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext,ProdutosPorCategoriaViewActivity.class);
                        Bundle args = new Bundle();
                        args.putSerializable(ProdutosPorCategoriaViewActivity.OBJ_BEBIDA, listaDeRefrigerantes);
                        intent.putExtra("BUNDLE",args);
                        mContext.startActivity(intent);
                    }
                });
                vhRefrigerante.listaBebidasRV.setAdapter(new Home_categorias_expandido_RVAdapter(mContext, listaDeRefrigerantes));
                vhRefrigerante.listaBebidasRV.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false));

                break;

            case 2:
                ListaVodkaViewHolder vhVodka = (ListaVodkaViewHolder) holder;
                vhVodka.nomeCategoria.setText(listaDeVodkas.get(0).getCategoria());
                vhVodka.mais.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext,ProdutosPorCategoriaViewActivity.class);
                        Bundle args = new Bundle();
                        args.putSerializable(ProdutosPorCategoriaViewActivity.OBJ_BEBIDA, listaDeVodkas);
                        intent.putExtra("BUNDLE",args);
                        mContext.startActivity(intent);
                    }
                });
                vhVodka.listaBebidasRV.setAdapter(new Home_categorias_expandido_RVAdapter(mContext, listaDeVodkas));
                vhVodka.listaBebidasRV.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false));

                break;
        }

    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: starts");
        return 3;   //numero de categorias diferentes
    }

    /**
     * Separa a lista inteira de bebidas por categorias que serão mostradas em listas diferentes.
     * @param listaDeBebidas lista de todas as bebidas
     */
    private void separarBebidas(ArrayList<Bebida> listaDeBebidas){
        Log.d(TAG, "separarBebidas: starts");
        for (Bebida b: listaDeBebidas) {
            switch (b.getCategoria()){
                case "Refrigerante":
                    listaDeRefrigerantes.add(b);
                    break;
                case "Vodka":
                    listaDeVodkas.add(b);
                    break;
                case "Cerveja":
                    listaDeCervejas.add(b);
                    break;
            }
        }
    }

}
