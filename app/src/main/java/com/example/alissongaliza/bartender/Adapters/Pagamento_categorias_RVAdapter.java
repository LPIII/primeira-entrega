package com.example.alissongaliza.bartender.Adapters;

import android.app.Activity;
import android.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.alissongaliza.bartender.View.Fragments.AdicionarEnderecosDialogFragment;
import com.example.alissongaliza.bartender.R;

/**
 * Created by Alisson on 17/01/2018.
 */

public class Pagamento_categorias_RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Activity mActivity;
    private Boolean agendamento = false;

    public Pagamento_categorias_RVAdapter(Activity activity, Boolean agendamento) {
        mActivity = activity;
        this.agendamento = agendamento;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(agendamento){
            switch (viewType){

                case 0:
                    //decidir sobre datepicker

                case 1:
                    View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagamento_pagueonline, parent, false);
                    return new PagueOnline(view1);

                case 2:
                    View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagamento_paguenaentrega, parent, false);
                    return new PagueNaEntrega(view2);

                case 3:
                    View view3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagamento_vouchercpf, parent, false);
                    return new VoucherCpf(view3);

                case 4:
                    View view4 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagamento_endereco, parent, false);
                    return new EndEntrega(view4);

                default:
                    View view5 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagamento_pagueonline, parent, false);
                    return new PagueOnline(view5);
            }
        }
        else{
            switch (viewType){

                case 0:
                    View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagamento_pagueonline, parent, false);
                    return new PagueOnline(view1);

                case 1:
                    View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagamento_paguenaentrega, parent, false);
                    return new PagueNaEntrega(view2);

                case 2:
                    View view3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagamento_vouchercpf, parent, false);
                    return new VoucherCpf(view3);

                case 3:
                    View view4 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagamento_endereco, parent, false);
                    return new EndEntrega(view4);

                default:
                    View view5 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagamento_pagueonline, parent, false);
                    return new PagueOnline(view5);
            }
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(agendamento){
//
        }

        else{
            switch (holder.getItemViewType()){

                case 0:

            }
        }
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private class PagueOnline extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView titulo;
        TextView informacoes;
        RadioButton masterPass;
        RadioButton visaCheckout;
        RadioButton cartaoDeCredito;
        RadioButton applePay;
        RadioButton googleWallet;


        PagueOnline(View itemView) {
            super(itemView);

            titulo = itemView.findViewById(R.id.pagamento_endEntrega_textView_titulo);
            informacoes = itemView.findViewById(R.id.pagamento_pagueonline_textView_informacoes);
            masterPass = itemView.findViewById(R.id.pagamento_pagueonline_radioButton_masterPass);
            visaCheckout = itemView.findViewById(R.id.pagamento_pagueonline_radioButton_visaCheckout);
            cartaoDeCredito = itemView.findViewById(R.id.pagamento_pagueonline_radioButton_cartaoDeCredito);
            applePay = itemView.findViewById(R.id.pagamento_pagueonline_radioButton_applePay);
            googleWallet = itemView.findViewById(R.id.pagamento_pagueonline_radioButton_googleWallet);

            masterPass.setOnClickListener(this);
            visaCheckout.setOnClickListener(this);
            cartaoDeCredito.setOnClickListener(this);
            applePay.setOnClickListener(this);
            googleWallet.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){

                case R.id.pagamento_pagueonline_radioButton_masterPass:
                    masterPass.setChecked(true);
                    visaCheckout.setChecked(false);
                    cartaoDeCredito.setChecked(false);
                    applePay.setChecked(false);
                    googleWallet.setChecked(false);
                    break;

                case R.id.pagamento_pagueonline_radioButton_visaCheckout:
                    masterPass.setChecked(false);
                    visaCheckout.setChecked(true);
                    cartaoDeCredito.setChecked(false);
                    applePay.setChecked(false);
                    googleWallet.setChecked(false);
                    break;

                case R.id.pagamento_pagueonline_radioButton_cartaoDeCredito:
                    masterPass.setChecked(false);
                    visaCheckout.setChecked(false);
                    cartaoDeCredito.setChecked(true);
                    applePay.setChecked(false);
                    googleWallet.setChecked(false);
                    break;

                case R.id.pagamento_pagueonline_radioButton_applePay:
                    masterPass.setChecked(false);
                    visaCheckout.setChecked(false);
                    cartaoDeCredito.setChecked(false);
                    applePay.setChecked(true);
                    googleWallet.setChecked(false);
                    break;

                case R.id.pagamento_pagueonline_radioButton_googleWallet:
                    masterPass.setChecked(false);
                    visaCheckout.setChecked(false);
                    cartaoDeCredito.setChecked(false);
                    applePay.setChecked(false);
                    googleWallet.setChecked(true);
                    break;


            }
        }
    }

    private class PagueNaEntrega extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView titulo;
        RadioButton creditoDebito;
        RadioButton dinheiro;


        PagueNaEntrega(View itemView) {
            super(itemView);
            titulo = itemView.findViewById(R.id.pagamento_endEntrega_textView_titulo);
            creditoDebito = itemView.findViewById(R.id.pagamento_paguenaentrega_radioButton_creditoDebito);
            dinheiro = itemView.findViewById(R.id.pagamento_paguenaentrega_radioButton_dinheiro);

            creditoDebito.setOnClickListener(this);
            dinheiro.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){

                case R.id.pagamento_paguenaentrega_radioButton_creditoDebito:
                    creditoDebito.setChecked(true);
                    dinheiro.setChecked(false);
                    break;

                case R.id.pagamento_paguenaentrega_radioButton_dinheiro:
                    creditoDebito.setChecked(false);
                    dinheiro.setChecked(true);
                    break;
            }
        }
    }

    private class VoucherCpf extends RecyclerView.ViewHolder{

        TextView tituloVoucher;
        TextView tituloCpf;
        EditText voucher;
        EditText cpf;

        VoucherCpf(View itemView) {
            super(itemView);
            tituloVoucher = itemView.findViewById(R.id.pagamento_endEntrega_textView_titulo);
            tituloCpf = itemView.findViewById(R.id.pagamento_voucherCpf_textView_tituloCpf);
            voucher = itemView.findViewById(R.id.pagamento_voucherCpf_editText_voucher);
            cpf = itemView.findViewById(R.id.pagamento_voucherCpf_editText_cpf);
        }
    }

    private class EndEntrega extends RecyclerView.ViewHolder implements View.OnClickListener{

        RadioGroup rgp;
        TextView titulo;
        TextView addEndereco;


        EndEntrega(View itemView) {
            super(itemView);
            titulo = itemView.findViewById(R.id.pagamento_endEntrega_textView_titulo);
            rgp = itemView.findViewById(R.id.pagamento_endereco_radioGroup);
            addEndereco = itemView.findViewById(R.id.pagamento_endereco_textView_addEndereco);

            addEndereco.setOnClickListener(this);

            RadioGroup.LayoutParams rprms;

            for(int i=0;i<3;i++){                       //TODO: Usar os dados cadastrados do usuario
                RadioButton radioButton = new RadioButton(itemView.getContext());
                radioButton.setText("new"+i);
                radioButton.setId(i);
                rprms= new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                rgp.addView(radioButton, rprms);
            }
        }

        @Override
        public void onClick(View view) {
            if(view.getId() == R.id.pagamento_endereco_textView_addEndereco){
                DialogFragment dialogFragment = new AdicionarEnderecosDialogFragment();
                dialogFragment.show(mActivity.getFragmentManager(), "AdicionarEnderecosDialogFragment");
            }
        }


    }
}
