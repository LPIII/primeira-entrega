package com.example.alissongaliza.bartender.Interfaces;

import com.example.alissongaliza.bartender.JavaClasses.Bebida;

/**
 * Created by Alisson on 11/04/18.
 */

public interface ProdutoDetalheContract {
    interface View extends BaseView<Presenter>{
        void respostaAdicionarAoCarrinho(String message);
        void atualizarView(Bebida bebida);
    }

    interface Presenter extends BasePresenter{
        void adicionarAoCarrinho(Bebida bebida);
        void menosUmClicked(Bebida bebida);
        void maisUmClicked(Bebida bebida);

    }
}
