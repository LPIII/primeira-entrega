package com.example.alissongaliza.bartender.JavaClasses;

import java.io.Serializable;


/**
 * Classe a ser extendida para as bebidas em geral
 */
public class Bebida implements Serializable{
    private String nome;
    private String descricao;
    private String categoria;
    private float preco;
    private String imagem;    //Caminho para a imagem ??
    private String harmonizacao;
    private String estilo_pais;
    private String cor;
    private String teorAlcoolico;
    private String temperatura;
    private int quantidadePedida;
    private String codigo;
    private int estoque;

    public Bebida(String nome, String descricao, String categoria, float preco, String imagem,
                  String harmonizacao, String estilo_pais, String cor, String teorAlcoolico, String temperatura) {
        this.nome = nome;
        this.descricao = descricao;
        this.categoria = categoria;
        this.preco = preco;
        this.imagem = imagem;
        this.harmonizacao = harmonizacao;
        this.estilo_pais = estilo_pais;
        this.cor = cor;
        this.teorAlcoolico = teorAlcoolico;
        this.temperatura = temperatura;
    }

    public Bebida(String nome, String descricao, String categoria, float preco, String imagem, String codigo, int estoque) {
        this.nome = nome;
        this.descricao = descricao;
        this.categoria = categoria;
        this.preco = preco;
        this.imagem = imagem;
        this.codigo = codigo;
        this.estoque = estoque;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public String getHarmonizacao() {
        return harmonizacao;
    }

    public void setHarmonizacao(String harmonizacao) {
        this.harmonizacao = harmonizacao;
    }

    public String getEstilo_pais() {
        return estilo_pais;
    }

    public void setEstilo_pais(String estilo_pais) {
        this.estilo_pais = estilo_pais;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getTeorAlcoolico() {
        return teorAlcoolico;
    }

    public void setTeorAlcoolico(String teorAlcoolico) {
        this.teorAlcoolico = teorAlcoolico;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    public int getQuantidadePedida() {
        return quantidadePedida;
    }

    public void setQuantidadePedida(int quantidadePedida) {
        this.quantidadePedida = quantidadePedida;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getEstoque() {
        return estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }

    @Override
    public String toString() {
        return "Bebida{" +
                "nome='" + nome + '\'' +
                ", descricao='" + descricao + '\'' +
                ", categoria='" + categoria + '\'' +
                ", preco=" + preco +
                ", imagem='" + imagem + '\'' +
                ", harmonizacao='" + harmonizacao + '\'' +
                ", estilo_pais='" + estilo_pais + '\'' +
                ", cor='" + cor + '\'' +
                ", teorAlcoolico='" + teorAlcoolico + '\'' +
                ", temperatura='" + temperatura + '\'' +
                ", quantidadePedida=" + quantidadePedida +
                ", codigo=" + codigo +
                ", estoque=" + estoque +
                '}';
    }
}
