package com.example.alissongaliza.bartender.Presenter;

import com.example.alissongaliza.bartender.Interfaces.MainContract;

/**
 * Created by ci on 11/04/18.
 */

public class MainPresenter implements MainContract.Presenter {

    MainContract.View view;

    public MainPresenter(MainContract.View view) {
        this.view = view;
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {
        //RxJava
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void buscarProduto(String query) {

    }

    @Override
    public void mudarEndereco(String endereco) {

    }
}
