package com.example.alissongaliza.bartender.Interfaces;

/**
 * Created by Alisson on 11/04/18.
 */

public interface PedidoDetalheContract {
    interface View extends BaseView<Presenter>{
    }

    interface Presenter extends BasePresenter{
    }
}
