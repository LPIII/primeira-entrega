package com.example.alissongaliza.bartender.View.Activities;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.alissongaliza.bartender.Adapters.Pedidos_detalhe_categorias_RVAdapter;
import com.example.alissongaliza.bartender.Interfaces.PedidoDetalheContract;
import com.example.alissongaliza.bartender.JavaClasses.Pedido;
import com.example.alissongaliza.bartender.R;

public class PedidoDetalheViewActivity extends AppCompatActivity implements PedidoDetalheContract.View{

    TextView numPedido;
    RecyclerView categoriasRV;

    public static final String OBJ_PEDIDO = "Pedido";

    PedidoDetalheContract.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos_detalhe);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        Pedido pedido = (Pedido) intent.getSerializableExtra(OBJ_PEDIDO);

        Resources res = getResources();

        numPedido = findViewById(R.id.pedidos_detalhe_textView_numPedido);
        categoriasRV = findViewById(R.id.pedidos_detalhe_RV);

        numPedido.setText(res.getString(R.string.pedidos_detalhe_textView_numPedido,pedido.getNumPedido()));
        categoriasRV.setAdapter(new Pedidos_detalhe_categorias_RVAdapter(this, pedido));
        categoriasRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return true;
    }

    @Override
    public void setPresenter(PedidoDetalheContract.Presenter presenterAbstraction) {
        this.presenter = presenterAbstraction;
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unsubscribe();
    }
}
