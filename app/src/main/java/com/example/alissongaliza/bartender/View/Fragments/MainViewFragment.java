package com.example.alissongaliza.bartender.View.Fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alissongaliza.bartender.Interfaces.MainContract;

import java.util.HashMap;

/**
 * Created by ci on 13/04/18.
 */

public class MainViewFragment extends Fragment implements MainContract.View{


    MainContract.Presenter presenter;

    @Override
    public void setPresenter(MainContract.Presenter presenterAbstraction) {
        this.presenter = presenterAbstraction;
    }

    @Override
    public void respostaBusca(HashMap<String, Object> listaDeBebidas) {

    }

    @Override
    public void enderecoNaoExiste(String message) {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public static MainViewFragment newInstance() {
        return new MainViewFragment();
    }
}
