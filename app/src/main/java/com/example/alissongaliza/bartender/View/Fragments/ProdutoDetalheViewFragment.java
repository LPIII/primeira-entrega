package com.example.alissongaliza.bartender.View.Fragments;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alissongaliza.bartender.Adapters.ProdutoDetalhe_RVAdapter;
import com.example.alissongaliza.bartender.Interfaces.ProdutoDetalheContract;
import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.R;
import com.example.alissongaliza.bartender.View.Activities.MainViewActivity;

import java.util.HashMap;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProdutoDetalheViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProdutoDetalheViewFragment extends Fragment implements View.OnClickListener,
                                                                    ProdutoDetalheContract.View{

    TextView nome;
    TextView precoTexto;
    TextView precoNumero;
    TextView quantidade;
    TextView totalTexto;
    TextView totalNumero;
    Button adicionar;
    Button remover;
    Button adicionarAoCarrinho;
    RecyclerView informacoes_comentarios;

    Bebida bebida;
    int qntd = 1;
    ProdutoDetalheContract.Presenter presenter;
    View view;

    public static final String OBJ_BEBIDA = "Obj bebida";
    public static final String CLASS_NAME = "ProdutoDetalheViewActivity";







    public ProdutoDetalheViewFragment() {
        // Required empty public constructor
    }

    public static ProdutoDetalheViewFragment newInstance(Bebida bebida){
        ProdutoDetalheViewFragment fragment = new ProdutoDetalheViewFragment();
        Bundle args = new Bundle();
        args.putSerializable(OBJ_BEBIDA, bebida);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =inflater.inflate(R.layout.fragment_produto_detalhe_view, container, false);

        if( getArguments() != null){
            bebida = (Bebida) getArguments().getSerializable(OBJ_BEBIDA);
        }
        setupWidgets(view, bebida);

        return view;


    }

    private void setupWidgets(View view, Bebida bebida){
        Resources resources = getResources();
        this.bebida = bebida;

        nome = view.findViewById(R.id.produto_detalhe_textView_nome);
        precoTexto = view.findViewById(R.id.produto_detalhe_textView_precoTexto);
        precoNumero = view.findViewById(R.id.produto_detalhe_textView_precoNumero);
        quantidade = view.findViewById(R.id.produto_detalhe_textView_quantidade);
        totalTexto = view.findViewById(R.id.produto_detalhe_textView_totalTexto);
        totalNumero = view.findViewById(R.id.produto_detalhe_textView_totalNumero);
        adicionar = view.findViewById(R.id.produto_detalhe_button_maisUm);
        remover = view.findViewById(R.id.produto_detalhe_button_menosUm);
        adicionarAoCarrinho = view.findViewById(R.id.produto_detalhe_button_adicionarAoCarrinho);

        adicionar.setOnClickListener(this);
        remover.setOnClickListener(this);
        adicionarAoCarrinho.setOnClickListener(this);
        nome.setText(bebida.getNome());
        precoNumero.setText(String.format(Locale.ENGLISH,"%.2f", bebida.getPreco()));
        quantidade.setText(String.valueOf(qntd));
        totalNumero.setText(String.format(Locale.ENGLISH
                , resources.getString(R.string.produto_detalhe_total_numero)
                , qntd * bebida.getPreco()));
        informacoes_comentarios = view.findViewById(R.id.produto_detalhe_recyclerView);
        informacoes_comentarios.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        informacoes_comentarios.setAdapter(new ProdutoDetalhe_RVAdapter(getContext(), bebida));
    }

    @Override
    public void onClick(View view) {

        Resources resources = getResources();
        switch (view.getId()){
            case R.id.produto_detalhe_button_maisUm:
                qntd++;
                precoNumero.setText(String.format(Locale.ENGLISH,"%.2f", bebida.getPreco()));
                quantidade.setText(String.valueOf(qntd));
                totalNumero.setText(String.format(Locale.ENGLISH
                        , resources.getString(R.string.produto_detalhe_total_numero)
                        , qntd * bebida.getPreco()));
                break;

            case R.id.produto_detalhe_button_menosUm:
                if(qntd > 0) {
                    qntd--;
                    precoNumero.setText(String.format(Locale.ENGLISH,"%.2f", bebida.getPreco()));
                    quantidade.setText(String.valueOf(qntd));
                    totalNumero.setText(String.format(Locale.ENGLISH
                            , resources.getString(R.string.produto_detalhe_total_numero)
                            , qntd * bebida.getPreco()));
                }
                break;

            case R.id.produto_detalhe_button_adicionarAoCarrinho:
                if(qntd > 0){
                    bebida.setQuantidadePedida(qntd);

                    presenter.adicionarAoCarrinho(bebida);

                }
                break;

        }
    }

    @Override
    public void setPresenter(ProdutoDetalheContract.Presenter presenterAbstraction) {
        this.presenter = presenterAbstraction;
    }

    @Override
    public void respostaAdicionarAoCarrinho(String message) {
        //TODO Mudar resposta do servidor para adicionado ao carrinho ou erro
        if(message.equalsIgnoreCase(getResources().getString(R.string.ENTRADA_STATUS_OK))){
            Toast.makeText(getContext(), "Adicionado ao carrinho", Toast.LENGTH_LONG).show();
            adicionadoAoCarrinho();
        }
        else{
            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void atualizarView(Bebida bebida) {
        setupWidgets(view, bebida);
    }

    /**
     * Protocolo de comunicação com o servidor. A comunicação com o servidor será por forma de hashmap.
     * @param bebida Bebida que será adicionada ao carrinho
     * @return hashmap a ser enviado para o servidor
     */
    private HashMap<String,Object> constroiMap(Bebida bebida){
        Resources res = getResources();
        HashMap<String,Object> map = new HashMap<>();
        map.put(res.getString(R.string.ENTRADA_ACAO), res.getString(R.string.ACAO_ADICIONAR_AO_CARRINHO));
        map.put(res.getString(R.string.ENTRADA_CLIENTE_NOME), "Alisson");            //Mudar quando tivermos contas concretas
//        map.put(res.getString(R.string.ENTRADA_BEBIDA_CODIGO), bebida.getCodigo());
        map.put(res.getString(R.string.ENTRADA_BEBIDA_CODIGO), "7894900700046");
        map.put(res.getString(R.string.ENTRADA_BEBIDA_QUANTIDADE), bebida.getQuantidadePedida());
        map.put(res.getString(R.string.KEY_CREDENCIAL_LOGIN), "admin");
        map.put(res.getString(R.string.KEY_CREDENCIAL_SENHA), "123");

        return map;
    }

    private void adicionadoAoCarrinho(){
        Intent intent = new Intent(getContext(), MainViewActivity.class);
        intent.putExtra(OBJ_BEBIDA, bebida);
        intent.putExtra("Class",CLASS_NAME);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.unsubscribe();
    }
}
