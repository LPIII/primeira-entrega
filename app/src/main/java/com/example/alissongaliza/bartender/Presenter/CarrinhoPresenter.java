package com.example.alissongaliza.bartender.Presenter;

import com.example.alissongaliza.bartender.Interfaces.CarrinhoContract;
import com.example.alissongaliza.bartender.JavaClasses.Bebida;

import java.util.ArrayList;

/**
 * Created by luiz on 11/04/18.
 */

public class CarrinhoPresenter implements CarrinhoContract.Presenter{

    CarrinhoContract.View view;
    private double precoTotal;
    private ArrayList<Bebida> bebidas;

    public CarrinhoPresenter(CarrinhoContract.View view, ArrayList<Bebida> bebidas) {
        this.view = view;
        this.view.setPresenter(this);
        precoTotal = 0;
        this.bebidas = bebidas;
    }

    @Override
    public void subscribe() {
        //RxJava
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void maisUmClicked(Bebida bebida) {
        for (Bebida b: bebidas) {
            if(b == bebida)
                b.setQuantidadePedida(b.getQuantidadePedida() + 1);
        }
        recalcularPedido(bebidas);
        view.atualizarLista();
    }

    @Override
    public void menosUmClicked(Bebida bebida) {
        for (Bebida b: bebidas) {
            if(b == bebida) {
                if(b.getQuantidadePedida() == 1)
                    this.view.removeBebida(b);
                else{
                    b.setQuantidadePedida(b.getQuantidadePedida() - 1);
                }
                view.atualizarLista();
                recalcularPedido(bebidas);
            }
        }
    }

    @Override
    public void finalizarPedidoClicked() {

    }

    @Override
    public void recalcularBebida(Bebida bebida) {

    }

    @Override
    public void recalcularPedido(ArrayList<Bebida> bebidas) {
        precoTotal = 0;
        for (Bebida b: bebidas) {
            precoTotal += b.getPreco() * b.getQuantidadePedida();
        }
        view.atualizarPrecoTotal(String.valueOf(precoTotal));
    }

    @Override
    public void recalcularFrete(String endereco) {

    }

}
