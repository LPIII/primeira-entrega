package com.example.alissongaliza.bartender.Model.Database;
/**
 *
 * @author Rhenan Castelo Branco
 * @autor Gabriel Belarmino
 */
public class POJO_Endereco implements Persistencia {
    
    
    private int idEnder; 
    private String cliente_cpfClien; 
    private String uf_pais_codPais; 
    private String uf_codUf; 
    private String cidaEnder; 
    private String ruaEnder;
    private String bairEnder;
    private String cepEnder;
    private String numeEnder;
    private String compEnder;

    public POJO_Endereco(int idEnder, String cliente_cpfClien, String uf_pais_codPais, String uf_codUf, String cidaEnder, String ruaEnder, String bairEnder, String cepEnder, String numeEnder, String compEnder) {
        super();
        this.idEnder = idEnder;
        this.cliente_cpfClien = cliente_cpfClien;
        this.uf_pais_codPais = uf_pais_codPais;
        this.uf_codUf = uf_codUf;
        this.cidaEnder = cidaEnder;
        this.ruaEnder = ruaEnder;
        this.bairEnder = bairEnder;
        this.cepEnder = cepEnder;
        this.numeEnder = numeEnder;
        this.compEnder = compEnder;
    }
    
    /**
     * Construtor sem o id autoincrementeavel
     * @param cliente_cpfClien
     * @param uf_pais_codPais
     * @param uf_codUf
     * @param cidaEnder
     * @param ruaEnder
     * @param bairEnder
     * @param cepEnder
     * @param numeEnder
     * @param compEnder 
     */
    public POJO_Endereco(String cliente_cpfClien, String uf_pais_codPais, String uf_codUf, String cidaEnder, String ruaEnder, String bairEnder, String cepEnder, String numeEnder, String compEnder) {
        this.cliente_cpfClien = cliente_cpfClien;
        this.uf_pais_codPais = uf_pais_codPais;
        this.uf_codUf = uf_codUf;
        this.cidaEnder = cidaEnder;
        this.ruaEnder = ruaEnder;
        this.bairEnder = bairEnder;
        this.cepEnder = cepEnder;
        this.numeEnder = numeEnder;
        this.compEnder = compEnder;
    }
      
    public POJO_Endereco() {}

    public int getIdEnder() {
        return idEnder;
    }

    public void setIdEnder(int idEnder) {
        this.idEnder = idEnder;
    }

    public String getCliente_cpfClien() {
        return cliente_cpfClien;
    }

    public void setCliente_cpfClien(String cliente_cpfClien) {
        this.cliente_cpfClien = cliente_cpfClien;
    }
    
    public String getUf_pais_codPais() {
        return uf_pais_codPais;
    }

    public void setUf_pais_codPais(String uf_pais_codPais) {
        this.uf_pais_codPais = uf_pais_codPais;
    }

    public String getUf_codUf() {
        return uf_codUf;
    }

    public void setUf_codUf(String uf_codUf) {
        this.uf_codUf = uf_codUf;
    }

    public String getCidaEnder() {
        return cidaEnder;
    }

    public void setCidaEnder(String cidaEnder) {
        this.cidaEnder = cidaEnder;
    }

    public String getRuaEnder() {
        return ruaEnder;
    }

    public void setRuaEnder(String ruaEnder) {
        this.ruaEnder = ruaEnder;
    }

    public String getBairEnder() {
        return bairEnder;
    }

    public void setBairEnder(String bairEnder) {
        this.bairEnder = bairEnder;
    }

    public String getCepEnder() {
        return cepEnder;
    }

    public void setCepEnder(String cepEnder) {
        this.cepEnder = cepEnder;
    }

    public String getNumeEnder() {
        return numeEnder;
    }

    public void setNumeEnder(String numeEnder) {
        this.numeEnder = numeEnder;
    }

    public String getCompEnder() {
        return compEnder;
    }

    public void setCompEnder(String compEnder) {
        this.compEnder = compEnder;
    }

    @Override
    public String toString () {
        return "Id: " +getIdEnder()+ "País: " +getUf_pais_codPais()+ "Uf: " +getUf_codUf()+"\n"
                +"Cidade: " +getCidaEnder()+ "Endereço: "+getRuaEnder()+", " +getNumeEnder()+"," +getBairEnder();

    }

    @Override
    public String getId() {
        return Integer.toString(idEnder);
    }

    @Override
    public void setId(String idEnder) {
        int x = Integer.parseInt(idEnder);
        this.idEnder = x;
    }
}
