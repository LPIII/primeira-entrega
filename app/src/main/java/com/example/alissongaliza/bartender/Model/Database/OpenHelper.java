package com.example.alissongaliza.bartender.Model.Database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Rhenan & Luiz on 11/04/18.
 * Fonte: <a href=https://developer.android.com/training/basics/data-storage/databases.html>ler aqui</a>
 */

//TODO: Verificar Sintaxe e Tipos do SQLite na construção do banco.

public class OpenHelper extends SQLiteOpenHelper {


    public static final String NOME_BANCO = "BartenderDatabase";
    public static final int VERSAO  = 1;

    public static OpenHelper instance;


    public OpenHelper(Context context) {
        super(context, NOME_BANCO, null, VERSAO);
    }

    public static OpenHelper getInstance (Context context) {
        if (instance == null)
            instance = new OpenHelper(context);
            return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL();

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }




}
