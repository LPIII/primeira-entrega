package com.example.alissongaliza.bartender.Interfaces;

/**
 * Created by Alisson on 10/04/18.
 */

public interface PedidosContract {
    interface View extends BaseView<Presenter>{

    }

    interface Presenter extends BasePresenter{
        void orderDetailsClicked(String idPedido);

    }
}
