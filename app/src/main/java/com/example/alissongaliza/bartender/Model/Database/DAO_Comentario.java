package com.example.alissongaliza.bartender.Model.Database;

import android.content.ContentValues;
import android.content.Context;

/**
 * Created by ci on 16/04/18.
 */

public class DAO_Comentario extends DAO<POJO_Comentario> {

    public DAO_Comentario(Context context) {
        super(context);
    }

    @Override
    public String getNomeColunaPrimaryKey() {
        return Gerencia_Comentario.IDCOMEN_COMENTARIO;
    }

    @Override
    public String getNomeTabela() {
        return Gerencia_Comentario.TAB_COMENTARIO;
    }

    @Override
    public ContentValues entidadeParacontentValues(POJO_Comentario entidade) {
        ContentValues values = new ContentValues();

        if (!entidade.getId().equals("0")){
            values.put(Gerencia_Comentario.IDCOMEN_COMENTARIO, entidade.getId());
        }

        values.put(Gerencia_Comentario.PRODUTO_CODIPRODU_COMENTARIO, entidade.getProduto_codiProdu());
        values.put(Gerencia_Comentario.DATACOMEN_COMENTARIO, entidade.getDataComen());
        values.put(Gerencia_Comentario.AVALCOMEN_COMENTARIO, entidade.getAvalComen());
        values.put(Gerencia_Comentario.COMECOMEN_COMENTARIO, entidade.getComeComen());
        values.put(Gerencia_Comentario.NOMECOMEN_COMENTARIO, entidade.getnomeClien());
        values.put(Gerencia_Comentario.IMAGCOMEN_COMENTARIO, entidade.getImagComen());

        return values;
    }

    @Override
    public POJO_Comentario contentValuesParaEntidade(ContentValues contentValues) {
        
        POJO_Comentario comentario  = new POJO_Comentario();
        comentario.setId(contentValues.getAsString(Gerencia_Comentario.IDCOMEN_COMENTARIO));
        comentario.setProduto_codiProdu(contentValues.getAsString(Gerencia_Comentario.PRODUTO_CODIPRODU_COMENTARIO));
        comentario.setDataComen(contentValues.getAsString(Gerencia_Comentario.DATACOMEN_COMENTARIO));
        comentario.setAvalComen(contentValues.getAsString(Gerencia_Comentario.AVALCOMEN_COMENTARIO));
        comentario.setComeComen(contentValues.getAsString(Gerencia_Comentario.COMECOMEN_COMENTARIO));
        comentario.setnomeClien(contentValues.getAsString(Gerencia_Comentario.NOMECOMEN_COMENTARIO));
        comentario.setImagComen(contentValues.getAsString(Gerencia_Comentario.IMAGCOMEN_COMENTARIO));

        return comentario;
    }
}
