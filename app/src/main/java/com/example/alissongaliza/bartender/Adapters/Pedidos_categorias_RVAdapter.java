package com.example.alissongaliza.bartender.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alissongaliza.bartender.JavaClasses.Pedido;
import com.example.alissongaliza.bartender.R;

import java.util.ArrayList;

/**
 * Created by Alisson on 03/01/2018.
 */

public class Pedidos_categorias_RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContext;
    ArrayList<Pedido> listaDePedidos;
    private ArrayList<Pedido> listaDePedidosAberto;
    private ArrayList<Pedido> listaDePedidosCancelado;
    private ArrayList<Pedido> listaDePedidosFinalizado;

    public Pedidos_categorias_RVAdapter(ArrayList<Pedido> listaDePedidos, Context context) {
        this.listaDePedidos = listaDePedidos;
        this.listaDePedidosAberto = new ArrayList<>();
        this.listaDePedidosCancelado = new ArrayList<>();
        this.listaDePedidosFinalizado = new ArrayList<>();
        separarPedidos(this.listaDePedidos);
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pedidos_categoria, parent, false);
        return new Status(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Status vh = (Status) holder;

        switch (holder.getItemViewType()){
            case 0:
                vh.status.setText(com.example.alissongaliza.bartender.Enum.Status.ABERTO.toString());
                vh.expandido.setAdapter(new Pedidos_categorias_expandido_RVAdapter(mContext, listaDePedidosAberto));
                break;
            case 1:
                vh.status.setText(com.example.alissongaliza.bartender.Enum.Status.FINALIZADO.toString());
                vh.expandido.setAdapter(new Pedidos_categorias_expandido_RVAdapter(mContext, listaDePedidosFinalizado));
                break;
            case 2:
                vh.status.setText(com.example.alissongaliza.bartender.Enum.Status.CANCELADO.toString());
                vh.expandido.setAdapter(new Pedidos_categorias_expandido_RVAdapter(mContext, listaDePedidosCancelado));
                break;
        }

        vh.expandido.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));



    }

    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Recebe uma lista de pedidos misturada e separa por status
     * @param listaDePedidos lista de pedidos geral
     */
    private void separarPedidos(ArrayList<Pedido> listaDePedidos){
        for (Pedido p: listaDePedidos) {
            switch (p.getStatus()){
                case ABERTO:
                    listaDePedidosAberto.add(p);
                    break;

                case CANCELADO:
                    listaDePedidosCancelado.add(p);
                    break;

                case FINALIZADO:
                    listaDePedidosFinalizado.add(p);
                    break;

            }
        }
    }


    private class Status extends RecyclerView.ViewHolder{
        TextView status;
        RecyclerView expandido;


        Status(View itemView) {
            super(itemView);
                status = itemView.findViewById(R.id.pedidos_categoria_textView_status);
                expandido = itemView.findViewById(R.id.pedidos_categoria_RV);

        }

    }


}
