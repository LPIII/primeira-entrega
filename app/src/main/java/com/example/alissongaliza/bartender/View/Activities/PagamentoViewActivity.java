package com.example.alissongaliza.bartender.View.Activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.alissongaliza.bartender.Adapters.Pagamento_categorias_RVAdapter;
import com.example.alissongaliza.bartender.R;

public class PagamentoViewActivity extends AppCompatActivity implements View.OnClickListener{

    Button receberAgora;
    Button agendarEntrega;
    Button finalizarPedido;
    RecyclerView categoriasRV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagamento);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        receberAgora = findViewById(R.id.pagamento_button_receberAgora);
        agendarEntrega = findViewById(R.id.pagamento_button_agendarEntrega);
        finalizarPedido = findViewById(R.id.pagamento_button_finalizarPedido);
        categoriasRV = findViewById(R.id.pagamento_categoriasRV);

        receberAgora.setOnClickListener(this);
        agendarEntrega.setOnClickListener(this);
        finalizarPedido.setOnClickListener(this);

        categoriasRV.setAdapter(new Pagamento_categorias_RVAdapter(this, false));
        categoriasRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.pagamento_button_receberAgora:
                receberAgora.setBackground(ContextCompat.getDrawable(this, R.drawable.retangulo_arredondado_marrom));
                receberAgora.setTextColor(getResources().getColor(R.color.fonte));
                agendarEntrega.setBackground(ContextCompat.getDrawable(this, R.drawable.retangulo_arredondado_fundo));
                agendarEntrega.setTextColor(getResources().getColor(R.color.colorPrimary));

                break;


            case R.id.pagamento_button_agendarEntrega:
                agendarEntrega.setBackground(ContextCompat.getDrawable(this, R.drawable.retangulo_arredondado_marrom));
                agendarEntrega.setTextColor(getResources().getColor(R.color.fonte));
                receberAgora.setBackground(ContextCompat.getDrawable(this, R.drawable.retangulo_arredondado_fundo));
                receberAgora.setTextColor(getResources().getColor(R.color.colorPrimary));

                break;

            case R.id.pagamento_button_finalizarPedido:



        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return true;
    }
}
