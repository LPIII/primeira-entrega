package com.example.alissongaliza.bartender.Interfaces;

import com.example.alissongaliza.bartender.JavaClasses.Bebida;

import java.util.ArrayList;

/**
 * Created by Alisson on 10/04/18.
 */

public interface CarrinhoContract {
    interface View extends BaseView<Presenter>{
        void atualizarLista();
        void atualizarPedido(ArrayList<Bebida> bebidas);
        void atualizarPrecoFrete(String frete);
        void atualizarPrecoTotal(String total);
        void removeBebida(Bebida bebida);
        void erroEndereco(String mensagemErro);
    }

    interface Presenter extends BasePresenter{
        void maisUmClicked(Bebida bebida);
        void menosUmClicked(Bebida bebida);
        void finalizarPedidoClicked();
        void recalcularBebida(Bebida bebida);
        void recalcularPedido(ArrayList<Bebida> bebidas);
        void recalcularFrete(String endereco);


    }
}
