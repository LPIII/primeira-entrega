package com.example.alissongaliza.bartender.Model.Database;

import android.content.ContentValues;
import android.provider.BaseColumns;

public class Gerencia_Usuario implements BaseColumns {
    public static final String TAB_USUARIO = "USUARIO";
    public static final String CPFUSUAR_USUARIO = "cpfClien";
    public static final String NOMEUSUAR_USUARIO = "nomeClien";
    public static final String SOBRUSUAR_USUARIO = "sobrClien";
    public static final String TELEUSUAR_USUARIO = "teleClien";
    public static final String IMAGUSUAR_USUARIO = "imagClien";
    public static final String EMAIUSUAR_USUARIO = "emaiClien";
    public static final String LOGIUSUAR_USUARIO = "logiClien";
    public static final String SENHUSUAR_USUARIO = "senhClien";
    public static final String NASCUSUAR_USUARIO = "nascClien";
    public static final String CRIAUSUAR_USUARIO = "criaClien";
    public static final String ATUAUSUAR_USUARIO = "atuaClien";

    public static String criaUSUARIO () {

        String SQL = "CREATE TABLE IF NOT EXISTS" +OpenHelper.NOME_BANCO+ "." +TAB_USUARIO+ "(" +
                CPFUSUAR_USUARIO  + " CHAR(11) NOT NULL," +
                NOMEUSUAR_USUARIO + " VARCHAR(20) NOT NULL," +
                SOBRUSUAR_USUARIO + " VARCHAR(60) NOT NULL," +
                TELEUSUAR_USUARIO + " VARCHAR(15) NOT NULL," +
                IMAGUSUAR_USUARIO + " TEXT(50) NULL," +
                EMAIUSUAR_USUARIO + " VARCHAR(50) NOT NULL," +
                LOGIUSUAR_USUARIO + " VARCHAR(20) NOT NULL," +
                SENHUSUAR_USUARIO + " VARCHAR(32) NOT NULL," +
                NASCUSUAR_USUARIO + " DATE NOT NULL,"+
                CRIAUSUAR_USUARIO + " DATETIME NOT NULL, " +
                ATUAUSUAR_USUARIO + " DATETIME NOT NULL, " +
                "PRIMARY KEY ("+CPFUSUAR_USUARIO+" ), " +
                "UNIQUE INDEX "+LOGIUSUAR_USUARIO+"_UNIQUE ("+LOGIUSUAR_USUARIO+" ASC)," +
                "UNIQUE INDEX "+EMAIUSUAR_USUARIO+"_UNIQUE ("+EMAIUSUAR_USUARIO+" ASC)," +
                "UNIQUE INDEX "+CPFUSUAR_USUARIO+"_UNIQUE ("+CPFUSUAR_USUARIO+" ASC)";

        return SQL;

    }

    protected ContentValues gerarContentValuesEndereco (POJO_Usuario usuario) {
        ContentValues values = new ContentValues();
        values.put(CPFUSUAR_USUARIO, usuario.getId());
        values.put(NOMEUSUAR_USUARIO, usuario.getNomeUsuar());
        values.put(SOBRUSUAR_USUARIO, usuario.getSobrUsuar());
        values.put(TELEUSUAR_USUARIO, usuario.getTeleUsuar());
        values.put(IMAGUSUAR_USUARIO, usuario.getImagUsuar());
        values.put(EMAIUSUAR_USUARIO, usuario.getEmaiUsuar());
        values.put(LOGIUSUAR_USUARIO, usuario.getLogiUsuar());
        values.put(SENHUSUAR_USUARIO, usuario.getSenhUsuar());
        values.put(NASCUSUAR_USUARIO, usuario.getNascUsuar());
        values.put(CRIAUSUAR_USUARIO, usuario.getCriaUsuar());
        values.put(ATUAUSUAR_USUARIO, usuario.getAtuaUsuar());

        return values;
    }
}