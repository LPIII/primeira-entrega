package com.example.alissongaliza.bartender.Interfaces;

import java.util.HashMap;

/**
 * Created by Alisson on 17/02/2018.
 */

public interface OnAddAddressResultsAvailable {
    void onAddAddressResultsAvailable(HashMap<String, Object> data);
}
