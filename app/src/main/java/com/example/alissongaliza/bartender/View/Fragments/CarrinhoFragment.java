package com.example.alissongaliza.bartender.View.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.alissongaliza.bartender.Adapters.Carrinho_RVAdapter;
import com.example.alissongaliza.bartender.Interfaces.CarrinhoContract;
import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.R;
import com.example.alissongaliza.bartender.View.Activities.MainViewActivity;

import java.util.ArrayList;

/**
 * Use the {@link CarrinhoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CarrinhoFragment extends Fragment implements Carrinho_RVAdapter.CarrinhoRV_ClickListener,
                                                            CarrinhoContract.View{

    TextView tempoEntrega;
    TextView precoEntrega;
    TextView totalPedidoNumero;
    TextView quantidade;
    TextView nomeMarca;
    Button adicionarMaisItens;
    Button finalizarPedido;
    RecyclerView pedidosRV;
    ArrayList<Bebida> bebidas;
    Carrinho_RVAdapter carrinho_RVAdapter;

    String precoTotal;

    CarrinhoContract.Presenter presenter;
    View view;
    public static final String BEBIDAS = "BEBIDAS";

    public CarrinhoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment CarrinhoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CarrinhoFragment newInstance(ArrayList<Bebida> bebidas) {
        CarrinhoFragment fragment = new CarrinhoFragment();
        Bundle args = new Bundle();
        args.putSerializable(BEBIDAS, bebidas);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_carrinho, container, false);
        if( getArguments() != null){
            bebidas = (ArrayList<Bebida>) getArguments().getSerializable(BEBIDAS);
        }

        setupWidgets(view, bebidas);

        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/beyond_the_mountains.ttf");
        nomeMarca.setTypeface(font);

        return view;
    }


    private void setupWidgets(View view, ArrayList<Bebida> listaBebidas){

        pedidosRV = view.findViewById(R.id.carrinho_recyclerView);
        finalizarPedido = view.findViewById(R.id.carrinho_button_finalizarPedido);
        totalPedidoNumero = view.findViewById(R.id.carrinho_textView_totalPedidoNumero);
        quantidade = view.findViewById(R.id.carrinho_pedidos_quantidade);
        nomeMarca = view.findViewById(R.id.main_textView_brand);
        adicionarMaisItens = view.findViewById(R.id.carrinho_button_adicionarMaisItens);

        adicionarMaisItens.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), MainViewActivity.class);
            startActivity(intent);
        });

        pedidosRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        pedidosRV.setAdapter(carrinho_RVAdapter);

        carrinho_RVAdapter.atualizaPedido(listaBebidas); //atualiza Pedidos do carrinho
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        carrinho_RVAdapter = new Carrinho_RVAdapter(this);
        presenter.subscribe();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.unsubscribe();
    }

    @Override
    public void setPresenter(CarrinhoContract.Presenter presenterAbstraction) {
        this.presenter = presenterAbstraction;
    }

    @Override
    public void atualizarLista() {
        carrinho_RVAdapter.notifyDataSetChanged();
    }

    @Override
    public void atualizarPedido(ArrayList<Bebida> bebidas) {

    }

    @Override
    public void atualizarPrecoFrete(String frete) {
        precoEntrega.setText(frete);
    }

    @Override
    public void atualizarPrecoTotal(String total) {
        totalPedidoNumero.setText(total);
    }

    @Override
    public void removeBebida(Bebida bebida) {
        for (int i = 0; i < bebidas.size(); i++){
            if(bebidas.get(i) == bebida)
               bebidas.remove(bebida);
            carrinho_RVAdapter.notifyItemRemoved(i);
            carrinho_RVAdapter.notifyItemRangeChanged(i, carrinho_RVAdapter.getItemCount());
        }

    }


    @Override
    public void onClick(View view, int position) {
        switch (view.getId()){
            case R.id.carrinho_pedidos_imageButton_adicionaUm:
                presenter.maisUmClicked(bebidas.get(position));
                break;

            case R.id.carrinho_pedidos_imageButton_removeUm:
                presenter.menosUmClicked(bebidas.get(position));
                break;


        }
    }

    @Override
    public void erroEndereco(String mensagemErro) {
    }
}
