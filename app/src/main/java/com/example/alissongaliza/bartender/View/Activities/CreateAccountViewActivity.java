package com.example.alissongaliza.bartender.View.Activities;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alissongaliza.bartender.Interfaces.CreateAccountContract;
import com.example.alissongaliza.bartender.R;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class CreateAccountViewActivity extends AppCompatActivity implements CreateAccountContract.View {

    private static final String TAG = "CreateAccountViewActivity";

    EditText nome;
    EditText sobrenome;
    EditText email;
    EditText telefone;
    EditText dataDeNascimento;
    EditText cpf;
    TextInputLayout senhaContainer;
    TextInputEditText senhaTexto;
    TextInputLayout confirmarSenhaContainer;
    TextInputEditText confirmarSenhaTexto;
    Button criarConta;
    View view;


    CreateAccountContract.Presenter presenter;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criar_conta);

        nome = findViewById(R.id.criarConta_editText_nome);
        sobrenome = findViewById(R.id.criarConta_editText_sobrenome);
        email = findViewById(R.id.criarConta_editText_email);
        telefone = findViewById(R.id.criarConta_editText_telefone);
        dataDeNascimento = findViewById(R.id.criarConta_editText_dataDeNascimento);
        cpf = findViewById(R.id.criarConta_editText_cpf);
        senhaContainer = findViewById(R.id.criarConta_textInputLayout_senha);
        senhaTexto = findViewById(R.id.criarConta_textInputEditText_senha);
        confirmarSenhaContainer = findViewById(R.id.criarConta_textInputLayout_confirmarSenha);
        confirmarSenhaTexto = findViewById(R.id.criarConta_textInputEditText_confirmarSenha);
        criarConta = findViewById(R.id.criarConta_button_criarConta);
        view = findViewById(R.id.view);

        criarConta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d((String)v.getTag(), "onClick: starts ");
                if(nome.getText().length() > 0 && email.getText().length() > 0 && nome.getText().length() > 0
                        && nome.getText().length() > 0 && nome.getText().length() > 0
                        && senhaTexto.getText().toString().equals(confirmarSenhaTexto.getText().toString())){
//                    new ComunicaComServer(CreateAccountViewActivity.this).execute(constroiMap());
                        presenter.createAccountClicked(constroiMap());
                }
            }
        });

        adicionarImagensAoLadoDoTexto();

    }

    private void adicionarImagensAoLadoDoTexto(){
        Drawable img;
        Resources res = getResources();

        img = res.getDrawable(R.mipmap.ic_criarconta_person);
        img.setBounds(0, 0, 50, 50);
        nome.setCompoundDrawables(img, null, null, null);
        nome.setCompoundDrawablePadding(0);
        nome.setPadding(10,0,0,15);

        img = res.getDrawable(R.mipmap.ic_criarconta_cpf);
        img.setBounds(0, 0, 50, 50);
        cpf.setCompoundDrawables(img, null, null, null);
        cpf.setCompoundDrawablePadding(0);
        cpf.setPadding(10,0,0,19);

        img = res.getDrawable(R.mipmap.ic_criarconta_email);
        img.setBounds(0, 0, 50, 50);
        email.setCompoundDrawables(img, null, null, null);
        email.setCompoundDrawablePadding(0);
        email.setPadding(10,0,0,19);

        img = res.getDrawable(R.mipmap.ic_criarconta_telefone);
        img.setBounds(0, 0, 50, 50);
        telefone.setCompoundDrawables(img, null, null, null);
        telefone.setCompoundDrawablePadding(0);
        telefone.setPadding(10,0,0,19);

        img = res.getDrawable(R.mipmap.ic_criarconta_datadenascimento);
        img.setBounds(0, 0, 40, 40);
        dataDeNascimento.setCompoundDrawables(img, null, null, null);
        dataDeNascimento.setCompoundDrawablePadding(5);
        dataDeNascimento.setPadding(13,0,0,9);

//        img = res.getDrawable(R.mipmap.ic_criarconta_senha);
//        img.setBounds(0, 0, 50, 50);
//        senhaTexto.setCompoundDrawables(img, null, null, null);
//        senhaTexto.setCompoundDrawablePadding(0);
//        senhaTexto.setPadding(10,0,0,14);

//        img = res.getDrawable(R.mipmap.ic_criarconta_senha);
//        img.setBounds(0, 0, 50, 50);
//        confirmarSenhaContainer.setCompoundDrawables(img, null, null, null);
//        confirmarSenhaContainer.setCompoundDrawablePadding(0);
//        confirmarSenhaContainer.setPadding(10,0,0,14);

    }


    /**
     * Protocolo de comunicação com o servidor. A comunicação com o servidor será por forma de hashmap.
     * @return hashmap a ser enviado para o servidor
     */
    private HashMap<String,Object> constroiMap(){
        Resources res = getResources();
        Date currentTime = Calendar.getInstance().getTime();
        Date data1= new Date(1996,5,17);


        HashMap<String,Object> map = new HashMap<>();
        map.put(res.getString(R.string.ENTRADA_ACAO), res.getString(R.string.ACAO_CADASTRAR_USUARIO));
        map.put(res.getString(R.string.ENTRADA_CLIENTE_NOME), nome.getText().toString());
        map.put(res.getString(R.string.ENTRADA_CLIENTE_SOBRENOME), sobrenome.getText().toString());
        map.put(res.getString(R.string.ENTRADA_CLIENTE_CPF), cpf.getText().toString());
//        map.put(res.getString(R.string.ENTRADA_CLIENTE_DATA_DE_NASCIMENTO), dataDeNascimento.getText());
        map.put(res.getString(R.string.ENTRADA_CLIENTE_DATA_DE_NASCIMENTO), data1);
        map.put(res.getString(R.string.ENTRADA_CLIENTE_SENHA), senhaTexto.getText().toString());
        map.put(res.getString(R.string.ENTRADA_CLIENTE_TELEFONE), telefone.getText().toString());
        map.put(res.getString(R.string.ENTRADA_CLIENTE_EMAIL), email.getText().toString());
        map.put(res.getString(R.string.ENTRADA_CLIENTE_LOGIN), email.getText().toString()); //TODO:!
        map.put(res.getString(R.string.ENTRADA_CLIENTE_DATA_CRIACAO), currentTime);
        map.put(res.getString(R.string.ENTRADA_CLIENTE_DATA_MODIFICACAO), currentTime);
        map.put(res.getString(R.string.KEY_CREDENCIAL_LOGIN), "admin");
        map.put(res.getString(R.string.KEY_CREDENCIAL_SENHA), "123");

        return map;
    }

    @Override
    public void onEmailError(String message) {
        //TODO
        email.setTextColor(Color.RED);
        Snackbar.make(view, "Email já utilizado",         //"view" nao testado, talvez possa passar o proprio textview msm
                Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void showConfirmationMessage() {
        Toast.makeText(this, "Conta criada com sucesso",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void setPresenter(CreateAccountContract.Presenter presenterAbstraction) {
        this.presenter = presenterAbstraction;
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.subscribe();
    }

    @Override
    protected void onPause() {
        presenter.unsubscribe();
        super.onPause();
    }
}
