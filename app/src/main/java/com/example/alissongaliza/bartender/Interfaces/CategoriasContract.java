package com.example.alissongaliza.bartender.Interfaces;

/**
 * Created by Alisson & Luiz on 10/04/18.
 */

public interface CategoriasContract {
    interface View extends BaseView<Presenter> {

    }

    interface Presenter extends BasePresenter {
        void categoryClicked(String category);

    }
}