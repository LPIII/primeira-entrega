package com.example.alissongaliza.bartender.Interfaces;

/**
 * Created by luiz on 11/04/18.
 */

public interface PerfilContract {

    interface View extends BaseView<Presenter> {

    }

    interface Presenter extends BasePresenter {
        void meusDadosClicked();
        void meusEnderecosClicked();
        void bartenderAmigoClicked();
        void pagamentoClicked();
        void avaliarClicked();
        void faqClicked();
        void fotoPerfilClicked();

    }
}