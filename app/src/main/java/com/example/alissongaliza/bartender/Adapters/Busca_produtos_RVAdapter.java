package com.example.alissongaliza.bartender.Adapters;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.R;

import java.util.ArrayList;

/**
 * Created by Eugenio on 22/01/2018.
 */

public class Busca_produtos_RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Bebida> bebidas;
    private Context mContext;

    public Busca_produtos_RVAdapter(Context context, ArrayList<Bebida> listaBebidas) {
        mContext = context;
        bebidas = listaBebidas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(bebidas.size() > 0){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.busca_produtos, parent, false);

            return new Resultados(view);
        }
        else{
            View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.busca_produtos_vazio, parent, false);

            return new Vazio(view2);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(bebidas.size() > 0) {
            Resultados vh = (Resultados) holder;

            if (bebidas.size() > 1) {
                vh.titulo.setText(mContext.getResources().getString(R.string.busca_produtos_textView_titulo, bebidas.size()));
            } else {
                String singular = "1 Resultado";
                vh.titulo.setText(singular);
            }

            vh.resultados.setAdapter(new Busca_produtos_expandido_RVAdapter(mContext, bebidas));
            vh.resultados.setLayoutManager(new GridLayoutManager(mContext,3));
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }



    private class Resultados extends RecyclerView.ViewHolder{
        RecyclerView resultados;
        TextView titulo;


        Resultados(View itemView) {
            super(itemView);

            resultados = itemView.findViewById(R.id.busca_produtos_RV);
            titulo = itemView.findViewById(R.id.busca_produtos_textView_titulo);
        }

    }

    private class Vazio extends RecyclerView.ViewHolder{
        TextView mensagem;

        Vazio(View itemView) {
            super(itemView);
            mensagem = itemView.findViewById(R.id.busca_produtos_vazio_textView_mensagem);
        }
    }
}
