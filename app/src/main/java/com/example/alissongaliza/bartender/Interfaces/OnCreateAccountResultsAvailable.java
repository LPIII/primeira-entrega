package com.example.alissongaliza.bartender.Interfaces;

import java.util.HashMap;

/**
 * Created by Alisson on 17/02/2018.
 */

public interface OnCreateAccountResultsAvailable {
    void onCreateAccountResultsAvailable(HashMap<String, Object> data, String usuario);
}
