package com.example.alissongaliza.bartender.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.R;

/**
 * Created by Alisson on 12/10/2017.
 */

public class ProdutoDetalhe_RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "ProdutoDetalhe_recycler";

    private Context mContext;
    private Bebida bebida;
    public ProdutoDetalhe_RVAdapter(Context context, Bebida bebida) {
        mContext = context;
        this.bebida = bebida;
    }

    private class Informacoes extends RecyclerView.ViewHolder{
        TextView titulo;
        TextView harmonizacao;
        TextView pais_estilo;
        TextView cor;
        TextView teorAlcoolico;
        TextView temperatura;

        Informacoes(View itemView) {
            super(itemView);
            titulo = (TextView) itemView.findViewById(R.id.produto_detalhe_informacao_titulo);
            harmonizacao = (TextView) itemView.findViewById(R.id.produto_detalhe_informacao_harmonizacao);
            pais_estilo = (TextView) itemView.findViewById(R.id.produto_detalhe_informacao_estiloPais);
            cor = (TextView) itemView.findViewById(R.id.produto_detalhe_informacao_cor);
            teorAlcoolico = (TextView) itemView.findViewById(R.id.produto_detalhe_informacao_teorAlcoolico);
            temperatura = (TextView) itemView.findViewById(R.id.produto_detalhe_informacao_temperatura);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.produto_detalhe_informacao, parent, false);
        return new Informacoes(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Informacoes vh = (Informacoes) holder;
        Resources resources = mContext.getResources();
        vh.harmonizacao.setText(resources.getString(R.string.produto_detalhe_informacao_harmonizacao,bebida.getHarmonizacao()));
        vh.pais_estilo.setText(resources.getString(R.string.produto_detalhe_informacao_estiloPais, bebida.getEstilo_pais()));
        vh.cor.setText(resources.getString(R.string.produto_detalhe_informacao_cor, bebida.getCor()));
        vh.teorAlcoolico.setText(resources.getString(R.string.produto_detalhe_informacao_teorAlcoolico, bebida.getTeorAlcoolico()));
        vh.temperatura.setText(resources.getString(R.string.produto_detalhe_informacao_temperatura, bebida.getTemperatura()));
    }

    @Override
    public int getItemCount() {
        return 1;
    }
}
