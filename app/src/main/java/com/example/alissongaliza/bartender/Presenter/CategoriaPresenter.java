package com.example.alissongaliza.bartender.Presenter;

import com.example.alissongaliza.bartender.Interfaces.CategoriasContract;

/**
 * Created by luiz on 11/04/18.
 */

public class CategoriaPresenter implements CategoriasContract.Presenter {

    CategoriasContract.Presenter presenter;

    public CategoriaPresenter(CategoriasContract.Presenter presenter) {
        this.presenter = presenter;
    }


    @Override
    public void subscribe() {
        //TODO: Se inscrever num evento(observável) com o RxJava
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void categoryClicked(String category) {

    }
}
