package com.example.alissongaliza.bartender.Model.Database;

import android.content.ContentValues;
import android.content.Context;

public class DAO_Cartao extends DAO <POJO_Cartao> {


    public DAO_Cartao(Context context) {
        super(context);
    }

    @Override
    public String getNomeColunaPrimaryKey() {
        return Gerencia_Cartao.NUMERO_CARTOES;
    }

    @Override
    public String getNomeTabela() {
        return Gerencia_Cartao.TAB_CARTOES;
    }

    @Override
    public ContentValues entidadeParacontentValues(POJO_Cartao entidade) {
        ContentValues values = new ContentValues();


        if (!entidade.getId().equals("")) {

            values.put(Gerencia_Cartao.NUMERO_CARTOES, entidade.getId());
        }
            values.put(Gerencia_Cartao.NOME_CARTOES, entidade.getNomeCarto());
            values.put(Gerencia_Cartao.VALIDADE_CARTOES, entidade.getValiCarto());
            values.put(Gerencia_Cartao.CVV_CARTOES, entidade.getCvvCarto());
            values.put(Gerencia_Cartao.CPF_CARTOES, entidade.getCpfCarto());
            values.put(Gerencia_Cartao.DEBITO_CARTOES, entidade.isDebiCarto());
            values.put(Gerencia_Cartao.CREDITO_CARTOES, entidade.isCredCarto());

        return values;

    }

    @Override
    public POJO_Cartao contentValuesParaEntidade(ContentValues contentValues) {

        POJO_Cartao cartao = new POJO_Cartao();

        cartao.setId(contentValues.getAsString(Gerencia_Cartao.NUMERO_CARTOES));
        cartao.setCpfCarto(contentValues.getAsString(Gerencia_Cartao.NOME_CARTOES));
        cartao.setValiCarto(contentValues.getAsString(Gerencia_Cartao.VALIDADE_CARTOES));
        cartao.setCvvCarto(contentValues.getAsString(Gerencia_Cartao.CVV_CARTOES));
        cartao.setCpfCarto(contentValues.getAsString(Gerencia_Cartao.CPF_CARTOES));
        cartao.setCredCarto(contentValues.getAsBoolean(Gerencia_Cartao.CREDITO_CARTOES));
        cartao.setDebiCarto(contentValues.getAsBoolean(Gerencia_Cartao.DEBITO_CARTOES));

        return cartao;
    }
}
