package com.example.alissongaliza.bartender.Presenter;

import com.example.alissongaliza.bartender.Interfaces.PedidoDetalheContract;

/**
 * Created by Alisson on 11/04/18.
 */

public class PedidoDetalhePresenter implements PedidoDetalheContract.Presenter {

    PedidoDetalheContract.View view;

    public PedidoDetalhePresenter(PedidoDetalheContract.View view) {
        this.view = view;
    }

    @Override
    public void subscribe() {
        //RxJava
    }

    @Override
    public void unsubscribe() {

    }
}
