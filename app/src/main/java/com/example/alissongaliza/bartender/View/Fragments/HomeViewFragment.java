package com.example.alissongaliza.bartender.View.Fragments;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.alissongaliza.bartender.Adapters.Busca_produtos_RVAdapter;
import com.example.alissongaliza.bartender.Adapters.Home_categorias_RVAdapter;
import com.example.alissongaliza.bartender.Interfaces.HomeContract;
import com.example.alissongaliza.bartender.Interfaces.OnSearchResultsAvailable;
import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.JavaClasses.ComunicaComServer;
import com.example.alissongaliza.bartender.Presenter.HomePresenter;
import com.example.alissongaliza.bartender.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeViewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeViewFragment extends Fragment implements HomeContract.View {

    private static final String TAG = "HomeViewFragment";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String LISTA_DE_BEBIDAS = "LISTA_DE_BEBIDAS";

    ArrayList<Bebida> listaDeBebidas;

    RecyclerView home_categorias_RV;
    Home_categorias_RVAdapter home_categorias_rvAdapter;
    Busca_produtos_RVAdapter busca_produtos_rvAdapter;
    SearchView buscador;

    HomeContract.Presenter presenter;           //ta errado isso, mas n sei como funciona com os fragmentos independentes em
    HomePresenter mPresenter;                   //BottomNavigationView

    public HomeViewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HomeViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeViewFragment newInstance() {
//        HomeViewFragment fragment = new HomeViewFragment();
//        Bundle args = new Bundle();
//        args.putSerializable(LISTA_DE_BEBIDAS, listaDeBebidas);
//        fragment.setArguments(args);
//        return fragment;
        return new HomeViewFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new HomePresenter(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        home_categorias_RV = view.findViewById(R.id.home_verticalRecyclerView);

        listaDeBebidas = new ArrayList<>();

        //teste dos RVs horizontal e vertical.

        //        listaDeBebidas =(ArrayList<Bebida>) getArguments().getSerializable(LISTA_DE_BEBIDAS);    //provisorio
        home_categorias_rvAdapter = new Home_categorias_RVAdapter(getContext(),
                populaListaDeBebidasTeste(listaDeBebidas));
        home_categorias_RV.setAdapter(home_categorias_rvAdapter);
        home_categorias_RV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        buscador = view.findViewById(R.id.searchView);
        buscador.clearFocus();
        buscador.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(getContext(), "Submited", Toast.LENGTH_SHORT).show();
//                ComunicaComServer server = new ComunicaComServer(HomeViewFragment.this, getActivity());
//                server.execute(constroiMap(query));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() == 0) {
                    if (listaDeBebidas.size() == 0) {
                        home_categorias_rvAdapter = new Home_categorias_RVAdapter(getContext(),
                                populaListaDeBebidasTeste(listaDeBebidas));
                        home_categorias_RV.setAdapter(home_categorias_rvAdapter);
                        home_categorias_RV.setLayoutManager(
                                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    } else {
                        home_categorias_rvAdapter = new Home_categorias_RVAdapter(getContext(), listaDeBebidas);
                        home_categorias_RV.setAdapter(home_categorias_rvAdapter);
                        home_categorias_RV.setLayoutManager(
                                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    }

                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.subscribe();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.unsubscribe();
    }

    @Override
    public void setPresenter(HomeContract.Presenter presenterAbstraction) {
        Log.d(TAG, "setPresenter: enters");
        this.presenter = presenterAbstraction;
    }

    private HashMap<String, Object> constroiMap(String query) {
        Resources res = getResources();

        HashMap<String, Object> map = new HashMap<>();
        map.put(res.getString(R.string.ENTRADA_ACAO), res.getString(R.string.ACAO_BUSCAR_PRODUTO));
        map.put(res.getString(R.string.KEY_CREDENCIAL_LOGIN), "admin");
        map.put(res.getString(R.string.KEY_CREDENCIAL_SENHA), "123");
        map.put(res.getString(R.string.ENTRADA_BEBIDA_NOME), query);

        return map;
    }

    /**
     * Cria bebidas para teste
     * @param lista lista para adicionar bebidas
     * @return lista com bebidas
     */
    private ArrayList<Bebida> populaListaDeBebidasTeste(ArrayList<Bebida> lista) {
        Bebida bebida1 = new Bebida("Coca-Cola", "Ótimo para desentupir pia", "Refrigerante", 3.50f, "caminho",
                "~Coisas que vao bem com coca~", "~estilo qqr~/ Azerbaijão", "Preto?", "0%", "10 a 18º");

        Bebida bebida2 = new Bebida("Heineken", "Cerveja ruim", "Cerveja", 7.50f, "caminho", "Amendoin",
                "~estilo qqr~/ Suíça", "Amarelo", "14%", "10 a 18º");

        Bebida bebida3 = new Bebida("Absolut", "Vodka slá", "Vodka", 17.50f, "caminho", "Morte", "~estilo qqr~/ USA",
                "Transparente", "24%", "10 a 18º");

        Bebida bebida4 = new Bebida("Pepsi Cola", "Pedir na falta de Coca", "Refrigerante", 3.50f, "caminho",
                "~Coisas que vao bem com coca~", "~estilo qqr~/ USA", "Preto", "0%", "10 a 18º");

        Bebida bebida5 = new Bebida("Dolly", "Seu amiguinho", "Refrigerante", 1.50f, "caminho", "~Recife~",
                "~estilo qqr~/ Brasil", "Preto", "0%", "10 a 18º");

        Bebida bebida6 = new Bebida("Fanta", "Preferido do Kel", "Refrigerante", 3.50f, "caminho",
                "~Coisas que vao bem com fanta~", "~estilo qqr~/ USA", "Laranja", "0%", "10 a 18º");

        Bebida bebida7 = new Bebida("Ciroc", "Nao sei", "Vodka", 33.50f, "caminho", "Morte", "~estilo qqr~/ França",
                "Transparente", "30%", "10 a 18º");

        Bebida bebida8 = new Bebida("Skol lata", "Redonda", "Cerveja", 2.50f, "caminho", "Bigode",
                "~estilo qqr~/ Brasil", "Amarelo", "10%", "10 a 18º");

        lista.add(bebida1);
        lista.add(bebida2);
        lista.add(bebida3);
        lista.add(bebida4);
        lista.add(bebida5);
        lista.add(bebida6);
        lista.add(bebida7);
        lista.add(bebida8);

        return lista;
    }

    /**
     * Extrai do hashmap enviado do servidor as bebidas buscadas a serem mostradas
     * @param data HashMap com a resposta bruta do servidor
     * @return Lista de bebidas buscadas
     */
    private ArrayList<Bebida> parseHashMap(HashMap<String, Object> data) {
        Bebida b;
        Resources res = getResources();
        ArrayList<HashMap<String, Object>> bebidasBrutas = (ArrayList<HashMap<String, Object>>) data
                .get("produtos_achados");
        ArrayList<Bebida> bebidasAchadas = new ArrayList<>();
        for (int i = 0; i < bebidasBrutas.size(); i++) {
            b = new Bebida((String) bebidasBrutas.get(i).get(res.getString(R.string.ENTRADA_BEBIDA_NOME)),
                    (String) bebidasBrutas.get(i).get(res.getString(R.string.ENTRADA_BEBIDA_DESCRICAO)),
                    (String) bebidasBrutas.get(i).get(res.getString(R.string.ENTRADA_BEBIDA_CATEGORIA)),
                    (float) bebidasBrutas.get(i).get(res.getString(R.string.ENTRADA_BEBIDA_PRECO)),
                    (String) bebidasBrutas.get(i).get(res.getString(R.string.ENTRADA_BEBIDA_IMAGEM)),
                    (String) bebidasBrutas.get(i).get(res.getString(R.string.ENTRADA_BEBIDA_CODIGO)),
                    (int) bebidasBrutas.get(i).get(res.getString(R.string.ENTRADA_BEBIDA_DISPONIBILIDADE)));
            bebidasAchadas.add(b);
        }
        return bebidasAchadas;
    }


}
