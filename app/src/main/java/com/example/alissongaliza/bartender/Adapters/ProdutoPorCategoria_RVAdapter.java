package com.example.alissongaliza.bartender.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alissongaliza.bartender.View.Activities.ProdutoDetalheViewActivity;
import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.R;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Alisson on 19/10/2017.
 */

public class ProdutoPorCategoria_RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "ProdutoPorCategoria_RVA";
    private Context mContext;
    private ArrayList<Bebida> bebidasPorCategoria;


    public ProdutoPorCategoria_RVAdapter(Context mContext, ArrayList<Bebida> bebidasPorCategoria) {
        this.mContext = mContext;
        this.bebidasPorCategoria = bebidasPorCategoria;
    }


    private class Produtos extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView bebida = null;
        TextView preco = null;
        TextView nome = null;

        Produtos(View itemView) {
            super(itemView);
            bebida = itemView.findViewById(R.id.home_expandido_imageView_fotoBebida);
            preco = itemView.findViewById(R.id.home_expandido_textView_preco);
            nome = itemView.findViewById(R.id.home_expandido_textView_nomeBebida);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext,ProdutoDetalheViewActivity.class);
            intent.putExtra(ProdutoDetalheViewActivity.OBJ_BEBIDA, bebidasPorCategoria.get(getAdapterPosition()));
            mContext.startActivity(intent);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: starts ");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_categorias_expandido, parent, false);
        return new Produtos(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: starts ");
        Produtos vh = (Produtos) holder;
        Resources resources = mContext.getResources();

        vh.nome.setText(bebidasPorCategoria.get(position).getNome());

        switch(bebidasPorCategoria.get(position).getNome()){
            case "Fanta":
                vh.bebida.setImageResource(R.mipmap.ic_fanta);
                break;
            case "Coca-Cola":
                vh.bebida.setImageResource(R.mipmap.ic_coca);
                break;
            case "Pepsi Cola":
                vh.bebida.setImageResource(R.mipmap.ic_pepsi);
                break;
            case "Dolly":
                vh.bebida.setImageResource(R.mipmap.ic_dolly);
                break;
            case "Ciroc":
                vh.bebida.setImageResource(R.mipmap.ic_ciroc);
                break;
            case "Absolut":
                vh.bebida.setImageResource(R.mipmap.ic_absolut);
                break;
            case "Skol lata":
                vh.bebida.setImageResource(R.mipmap.ic_skol);
                break;
            case "Heineken":
                vh.bebida.setImageResource(R.mipmap.ic_heineken);
                break;


        }
        vh.preco.setText(String.format(Locale.ENGLISH,resources.getString(R.string.home_expandido_preco), bebidasPorCategoria.get(position).getPreco()));

    }

    @Override
    public int getItemCount() {
        return bebidasPorCategoria.size();
    }


}
