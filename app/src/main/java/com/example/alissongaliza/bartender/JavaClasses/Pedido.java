package com.example.alissongaliza.bartender.JavaClasses;

import com.example.alissongaliza.bartender.Enum.Status;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Alisson on 11/01/2018.
 */

public class Pedido implements Serializable{
    private Status status;
    private String nfe;
    private String enderecoEntrega;
    private String data;
    private String hora;
    private ArrayList<Bebida> listaBebidas;
    private String metodoDePagamento;
    private String cartao;
    private float totalSemDesconto;
    private float frete;
    private float desconto;
    private float totalComDesconto;
    private int numPedido;

    public Pedido(Status status, String nfe, String enderecoEntrega, String data, String hora,
                  ArrayList<Bebida> listaBebidas, String metodoDePagamento, String cartao,
                  float totalSemDesconto, float frete, float desconto, float totalComDesconto,
                  int numPedido) {

        this.status = status;
        this.nfe = nfe;
        this.enderecoEntrega = enderecoEntrega;
        this.data = data;
        this.hora = hora;
        this.listaBebidas = listaBebidas;
        this.metodoDePagamento = metodoDePagamento;
        this.cartao = cartao;
        this.totalSemDesconto = totalSemDesconto;
        this.frete = frete;
        this.desconto = desconto;
        this.totalComDesconto = totalComDesconto;
        this.numPedido = numPedido;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getNfe() {
        return nfe;
    }

    public void setNfe(String nfe) {
        this.nfe = nfe;
    }

    public String getEnderecoEntrega() {
        return enderecoEntrega;
    }

    public void setEnderecoEntrega(String enderecoEntrega) {
        this.enderecoEntrega = enderecoEntrega;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public ArrayList<Bebida> getListaBebidas() {
        return listaBebidas;
    }

    public void setListaBebidas(ArrayList<Bebida> listaBebidas) {
        this.listaBebidas = listaBebidas;
    }

    public String getMetodoDePagamento() {
        return metodoDePagamento;
    }

    public void setMetodoDePagamento(String metodoDePagamento) {
        this.metodoDePagamento = metodoDePagamento;
    }

    public String getCartao() {
        return cartao;
    }

    public void setCartao(String cartao) {
        this.cartao = cartao;
    }

    public float getTotalSemDesconto() {
        return totalSemDesconto;
    }


    public void setTotalSemDesconto(float totalSemDesconto) {
        this.totalSemDesconto = totalSemDesconto;
    }

    public float getFrete() {
        return frete;
    }

    public void setFrete(float frete) {
        this.frete = frete;
    }

    public float getDesconto() {
        return desconto;
    }

    public void setDesconto(float desconto) {
        this.desconto = desconto;
    }

    public float getTotalComDesconto() {
        return totalComDesconto;
    }

    public void setTotalComDesconto(float totalComDesconto) {
        this.totalComDesconto = totalComDesconto;
    }

    public int getNumPedido() {
        return numPedido;
    }

    public void setNumPedido(int numPedido) {
        this.numPedido = numPedido;
    }
}
