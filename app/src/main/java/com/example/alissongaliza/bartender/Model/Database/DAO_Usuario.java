package com.example.alissongaliza.bartender.Model.Database;

import android.content.ContentValues;
import android.content.Context;

public class DAO_Usuario extends DAO<POJO_Usuario> {


    public DAO_Usuario(Context context) {
        super(context);
    }

    @Override
    public String getNomeColunaPrimaryKey() {
        return Gerencia_Usuario.CPFUSUAR_USUARIO;
    }

    @Override
    public String getNomeTabela() {
        return Gerencia_Usuario.TAB_USUARIO;
    }

    @Override
    public ContentValues entidadeParacontentValues(POJO_Usuario entidade) {

        ContentValues values = new ContentValues();

        if (!entidade.getId().equals("0")) {
            values.put(Gerencia_Usuario.CPFUSUAR_USUARIO,entidade.getId());
        }
            values.put(Gerencia_Usuario.NOMEUSUAR_USUARIO,entidade.getNomeUsuar());
            values.put(Gerencia_Usuario.SOBRUSUAR_USUARIO,entidade.getSobrUsuar());
            values.put(Gerencia_Usuario.TELEUSUAR_USUARIO,entidade.getTeleUsuar());
            values.put(Gerencia_Usuario.IMAGUSUAR_USUARIO,entidade.getImagUsuar());
            values.put(Gerencia_Usuario.EMAIUSUAR_USUARIO,entidade.getEmaiUsuar());
            values.put(Gerencia_Usuario.LOGIUSUAR_USUARIO,entidade.getLogiUsuar());
            values.put(Gerencia_Usuario.SENHUSUAR_USUARIO,entidade.getSenhUsuar());
            values.put(Gerencia_Usuario.NASCUSUAR_USUARIO,entidade.getNascUsuar());
            values.put(Gerencia_Usuario.CRIAUSUAR_USUARIO,entidade.getCriaUsuar());
            values.put(Gerencia_Usuario.ATUAUSUAR_USUARIO,entidade.getAtuaUsuar());

            return values;
    }

    @Override
    public POJO_Usuario contentValuesParaEntidade(ContentValues contentValues) {
        return null;
    }
}
