package com.example.alissongaliza.bartender.Model.Database;

import android.content.ContentValues;
import android.provider.BaseColumns;

public class Gerencia_Produto implements BaseColumns {
    public static final String TAB_PRODUTO = "produto";
    public static final String CODIPRODU_PRODUTO = "codiProdu";
    public static final String CATEGORIA_IDCATEG_PRODUTO = "categoria_idCateg";
    public static final String NOMEPRODU_PRODUTO = "nomeProdu";
    public static final String DISPPRODU_PRODUTO = "dispProdu";
    public static final String PRECPRODU_PRODUTO = "precProdu";
    public static final String DESCPRODU_PRODUTO = "descProdu";
    public static final String IMAGPRODU_PRODUTO = "imagProdu";
    public static final String QUANPRODU_PRODUTO = "quanProdu";
    public static final String ATUAPRODU_PRODUTO = "atuaProdu";
    public static final String AVALPRODU_PRODUTO = "avalProdu";

    public String createTable_Produto() {
        String TAB_PRODUTO_CRIAR = "CREATE TABLE " + Gerencia_Produto.TAB_PRODUTO + " (" +
                Gerencia_Produto.TAB_PRODUTO + " CreateDatabase." +
                Gerencia_Produto.CODIPRODU_PRODUTO + "CreateDatabase." +
                Gerencia_Produto.CATEGORIA_IDCATEG_PRODUTO + " int" +
                Gerencia_Produto.NOMEPRODU_PRODUTO + "CreateDatabase." +
                Gerencia_Produto.DISPPRODU_PRODUTO + "int" +
                Gerencia_Produto.PRECPRODU_PRODUTO + "float" +
                Gerencia_Produto.DESCPRODU_PRODUTO + "CreateDatabase." +
                Gerencia_Produto.IMAGPRODU_PRODUTO + "CreateDatabase." +
                Gerencia_Produto.QUANPRODU_PRODUTO + "int" +
                Gerencia_Produto.ATUAPRODU_PRODUTO + "CreateDatabase." +
                Gerencia_Produto.AVALPRODU_PRODUTO + "float"
                + ")";
        return TAB_PRODUTO_CRIAR;
    }

    protected ContentValues gerarContentValuesEndereco (POJO_Produto produto) {
        ContentValues values = new ContentValues();
        values.put(CODIPRODU_PRODUTO, produto.getId());
        values.put(CATEGORIA_IDCATEG_PRODUTO, produto.getCategoria_idCateg());
        values.put(NOMEPRODU_PRODUTO, produto.getNomeProdu());
        values.put(DISPPRODU_PRODUTO, produto.getDispProdu());
        values.put(PRECPRODU_PRODUTO, produto.getPrecProdu());
        values.put(DESCPRODU_PRODUTO, produto.getDescProdu());
        values.put(IMAGPRODU_PRODUTO, produto.getImagProdu());
        values.put(QUANPRODU_PRODUTO, produto.getQuantProdu());
        values.put(ATUAPRODU_PRODUTO, produto.getQuantProdu());
        values.put(AVALPRODU_PRODUTO, produto.getAvalProdu());

        return values;
    }
}