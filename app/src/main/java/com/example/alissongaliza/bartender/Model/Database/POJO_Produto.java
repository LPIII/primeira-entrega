package com.example.alissongaliza.bartender.Model.Database;


/**
 *
 * @author Rhenan Castelo Branco
 */
public class POJO_Produto implements Persistencia{

    private String codiProdu;
    private String nomeProdu;
    private String categoria_idCateg;
    private int dispProdu;
    private float precProdu;
    private String descProdu;
    private String imagProdu;
    private int quantProdu;
    private String atuaProdu;
    private float avalProdu;

    
    public POJO_Produto(String codiProdu, String nomeProdu, String categoria_idCateg, int dispProdu, float precProdu, String descProdu, String imagProdu, int quantProdu, String atuaProdu, String link, Float avalProdu) {
        super();
        this.codiProdu = codiProdu;
        this.nomeProdu = nomeProdu;
        this.categoria_idCateg = categoria_idCateg;
        this.dispProdu = dispProdu;
        this.precProdu = precProdu;
        this.descProdu = descProdu;
        this.imagProdu = imagProdu;
        this.quantProdu = quantProdu;
        this.atuaProdu = atuaProdu;
        this.avalProdu = avalProdu;
    }


    public POJO_Produto(String codiProdu, String nomeProdu, String categoria_idCateg, int dispProdu, float precProdu, String descProdu, String imagProdu, int quantProdu, String atuaProdu, float avalProdu) {
        this.codiProdu = codiProdu;
        this.nomeProdu = nomeProdu;
        this.categoria_idCateg = categoria_idCateg;
        this.dispProdu = dispProdu;
        this.precProdu = precProdu;
        this.descProdu = descProdu;
        this.imagProdu = imagProdu;
        this.quantProdu = quantProdu;
        this.atuaProdu = atuaProdu;
        this.avalProdu = avalProdu;
    }


    public float getAvalProdu() {
        return avalProdu;
    }

    public void setAvalProdu(float avalProdu) {
        this.avalProdu = avalProdu;
    }
    
    public String getCodiProdu() {
        return codiProdu;
    }

    public void setCodiProdu(String codiProdu) {
        this.codiProdu = codiProdu;
    }

    public String getNomeProdu() {
        return nomeProdu;
    }

    public void setNomeProdu(String nomeProdu) {
        this.nomeProdu = nomeProdu;
    }

    public String getCategoria_idCateg() {
        return categoria_idCateg;
    }

    public void setCategoria_idCateg(String categoria_idCateg) {
        this.categoria_idCateg = categoria_idCateg;
    }

    public int getDispProdu() {
        return dispProdu;
    }

    public void setDispProdu(int dispProdu) {
        this.dispProdu = dispProdu;
    }

    public float getPrecProdu() {
        return precProdu;
    }

    public void setPrecProdu(float precProdu) {
        this.precProdu = precProdu;
    }

    public String getDescProdu() {
        return descProdu;
    }

    public void setDescProdu(String descProdu) {
        this.descProdu = descProdu;
    }

    public String getImagProdu() {
        return imagProdu;
    }

    public void setImagProdu(String imagProdu) {
        this.imagProdu = imagProdu;
    }

    public int getQuantProdu() {
        return quantProdu;
    }

    public void setQuantProdu(int quantProdu) {
        this.quantProdu = quantProdu;
    }

    public String getAtuaProdu() {
        return atuaProdu;
    }

    public void setAtuaProdu(String atuaProdu) {
        this.atuaProdu = atuaProdu;
    }


    @Override
    public String getId() {
        return codiProdu;
    }

    @Override
    public void setId(String codiProdu) {
    this.codiProdu = codiProdu;
    }
}
