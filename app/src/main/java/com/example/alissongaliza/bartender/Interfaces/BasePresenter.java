package com.example.alissongaliza.bartender.Interfaces;

/**
 * Created by Alisson on 08/04/2018.
 */

public interface BasePresenter {
    void subscribe();
    void unsubscribe();
}
