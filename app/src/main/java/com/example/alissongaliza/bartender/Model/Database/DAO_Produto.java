package com.example.alissongaliza.bartender.Model.Database;

import android.content.ContentValues;
import android.content.Context;

public class DAO_Produto extends DAO<POJO_Produto> {


    public DAO_Produto(Context context) {
        super(context);
    }

    @Override
    public String getNomeColunaPrimaryKey() {

        return Gerencia_Produto.CODIPRODU_PRODUTO;
    }

    @Override
    public String getNomeTabela() {
        return Gerencia_Produto.TAB_PRODUTO;
    }

    @Override
    public ContentValues entidadeParacontentValues(POJO_Produto entidade) {

        ContentValues values = new ContentValues();
            if (!entidade.equals("")) {
                values.put(Gerencia_Produto.CODIPRODU_PRODUTO, entidade.getCodiProdu());
            }
            values.put(Gerencia_Produto.CATEGORIA_IDCATEG_PRODUTO, entidade.getCategoria_idCateg());
            values.put(Gerencia_Produto.NOMEPRODU_PRODUTO, entidade.getNomeProdu());
            values.put(Gerencia_Produto.DISPPRODU_PRODUTO, entidade.getDispProdu());
            values.put(Gerencia_Produto.PRECPRODU_PRODUTO, entidade.getPrecProdu());
            values.put(Gerencia_Produto.DESCPRODU_PRODUTO, entidade.getDescProdu());
            values.put(Gerencia_Produto.IMAGPRODU_PRODUTO, entidade.getImagProdu());
            values.put(Gerencia_Produto.QUANPRODU_PRODUTO, entidade.getQuantProdu());
            values.put(Gerencia_Produto.ATUAPRODU_PRODUTO, entidade.getQuantProdu());
            values.put(Gerencia_Produto.AVALPRODU_PRODUTO, entidade.getAvalProdu());

        return values;
    }

    @Override
    public POJO_Produto contentValuesParaEntidade(ContentValues contentValues) {
        return null;
    }
}
