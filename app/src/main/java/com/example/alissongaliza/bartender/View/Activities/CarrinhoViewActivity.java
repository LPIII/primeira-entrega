package com.example.alissongaliza.bartender.View.Activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alissongaliza.bartender.Adapters.Carrinho_RVAdapter;
import com.example.alissongaliza.bartender.Interfaces.CarrinhoContract;
import com.example.alissongaliza.bartender.JavaClasses.Bebida;
import com.example.alissongaliza.bartender.Presenter.CarrinhoPresenter;
import com.example.alissongaliza.bartender.R;
import com.example.alissongaliza.bartender.View.Fragments.CarrinhoFragment;
import com.example.alissongaliza.bartender.View.Fragments.ProdutoDetalheViewFragment;

import java.util.ArrayList;

public class CarrinhoViewActivity extends AppCompatActivity implements MainViewActivity.AdicionarAoCarrinho{

    ArrayList<Bebida> bebidas;

    public static final String CLASS_NAME = "CarrinhoViewActivity";

    CarrinhoPresenter presenter;

    public CarrinhoViewActivity() {
        bebidas = new ArrayList<>();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrinho);

        Intent intent = getIntent();
        adicionaAoCarrinho((ArrayList<Bebida>) intent.getSerializableExtra(MainViewActivity.ARRAYLIST_DE_BEBIDAS));

        CarrinhoFragment fragment =
                ( CarrinhoFragment ) getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment == null) {
            fragment = CarrinhoFragment.newInstance(bebidas);
        }
        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.content, fragment).
                commit();
        presenter = new CarrinhoPresenter(fragment, bebidas);


    }





    @Override
    public void adicionaAoCarrinho(ArrayList<Bebida> bebidas) {
        this.bebidas.addAll(bebidas);
    }



    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        broadcastMessage();
        super.onBackPressed();

    }



    private void broadcastMessage(){
        Intent intent = new Intent(MainViewActivity.ATUALIZAR_LISTA_DE_BEBIDAS);
        intent.putExtra(MainViewActivity.ARRAYLIST_DE_BEBIDAS, bebidas);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
