package com.example.alissongaliza.bartender.JavaClasses;

import android.app.Activity;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.alissongaliza.bartender.View.Activities.MainViewActivity;
import com.example.alissongaliza.bartender.View.Activities.ProdutoDetalheViewActivity;
import com.example.alissongaliza.bartender.Interfaces.OnAddAddressResultsAvailable;
import com.example.alissongaliza.bartender.Interfaces.OnAddCarrinhoResultsAvailable;
import com.example.alissongaliza.bartender.Interfaces.OnCreateAccountResultsAvailable;
import com.example.alissongaliza.bartender.Interfaces.OnLoginResultsAvailable;
import com.example.alissongaliza.bartender.Interfaces.OnSearchResultsAvailable;
import com.example.alissongaliza.bartender.R;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Alisson on 04/01/2018.
 */

public class ComunicaComServer extends AsyncTask<HashMap<String,Object>, Void, Void>{

    private static final String TAG = "ComunicaComServer";

    private WeakReference<OnSearchResultsAvailable> mBuscaListener;
    private WeakReference<OnLoginResultsAvailable> mLoginListener;
    private WeakReference<OnCreateAccountResultsAvailable> mCreateListener;
    private WeakReference<OnAddAddressResultsAvailable> mAddAddressListener;
    private WeakReference<OnAddCarrinhoResultsAvailable> mAddCarrinhoListener;
    private WeakReference<Activity> mActivity;

    private Socket socketGerente;
    /** variavel para mandar msgs para o servidor*/
    private ObjectOutputStream out;
    /** Receber as msgs do servidor */
    private ObjectInputStream in;
    /** Receber a resposta em forma de map */
    private HashMap<String, Object> mapResposta;

    private HashMap<String, Object> mapEnviado;




//    ComunicaComServer(Activity mActivity) {
//        this.weakActivity = new WeakReference<>(mActivity);
//    }

    public ComunicaComServer(OnLoginResultsAvailable mListener, Activity mActivity){
        this.mLoginListener = new WeakReference<>(mListener);
        this.mActivity = new WeakReference<>(mActivity);
    }

    public ComunicaComServer(OnCreateAccountResultsAvailable mListener, Activity mActivity){
        this.mCreateListener = new WeakReference<>(mListener);
        this.mActivity = new WeakReference<>(mActivity);
    }

    public ComunicaComServer(OnSearchResultsAvailable mListener, Activity mActivity){
        this.mBuscaListener = new WeakReference<>(mListener);
        this.mActivity = new WeakReference<>(mActivity);
    }

    public ComunicaComServer(OnAddAddressResultsAvailable mListener, Activity mActivity){
        this.mAddAddressListener = new WeakReference<>(mListener);
        this.mActivity = new WeakReference<>(mActivity);
    }

    public ComunicaComServer(OnAddCarrinhoResultsAvailable mListener, Activity mActivity){
        this.mAddCarrinhoListener = new WeakReference<>(mListener);
        this.mActivity = new WeakReference<>(mActivity);
    }


    @Override
    protected  Void doInBackground(HashMap<String, Object>[] hashMaps) {
//        if(isNetworkAvailable()) {
        mapEnviado = hashMaps[0];
        final Activity activity = mActivity.get();
        comunicaComOServer();
        if(out != null){
            enviarMsg(hashMaps[0]);
        }
        else{
            activity.runOnUiThread(new Runnable() {
                public void run() {

                    Toast.makeText(activity, "Erro de conexão - Mensagem nao enviada", Toast.LENGTH_SHORT).show();
                }
            });
        }
//        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {

        Activity activity = mActivity.get();
        if (activity == null
                || activity.isFinishing()
                || activity.isDestroyed()
                ) {
            return;
        }

        if(mapResposta != null){

            Resources res = mActivity.get().getResources();
            String criarConta = res.getString(R.string.ACAO_CADASTRAR_USUARIO);
            String buscarProduto = res.getString(R.string.ACAO_BUSCAR_PRODUTO);
            String login = activity.getString(R.string.ACAO_FAZER_LOGIN);
            String addEndereco = activity.getString(R.string.ACAO_CADASTRAR_ENDERECO);
            String addCarrinho = activity.getString(R.string.ACAO_ADICIONAR_AO_CARRINHO);
            String tipoDeRequisicao =(String) mapEnviado.get(activity.getResources().getString(R.string.ENTRADA_ACAO));

            if(tipoDeRequisicao.equals(login)){
                final OnLoginResultsAvailable mLogin = mLoginListener.get();
                mLogin.onLoginResultsAvailable(mapResposta);
            }

            else if(tipoDeRequisicao.equals(criarConta)){
                final OnCreateAccountResultsAvailable mCreate = mCreateListener.get();
                mCreate.onCreateAccountResultsAvailable(mapResposta, (String) mapEnviado.get(res.getString(R.string.ENTRADA_CLIENTE_EMAIL)));

            }
            else if(tipoDeRequisicao.equals(buscarProduto)){
                final OnSearchResultsAvailable mSearch = mBuscaListener.get();
                mSearch.onSearchResultsAvailable(mapResposta);
            }
            else if(tipoDeRequisicao.equals(addEndereco)){
                final OnAddAddressResultsAvailable mSearch = mAddAddressListener.get();
                mSearch.onAddAddressResultsAvailable(mapResposta);
            }
            else if(tipoDeRequisicao.equals(addCarrinho)){
                final OnAddCarrinhoResultsAvailable mSearch = mAddCarrinhoListener.get();
                mSearch.onAddCarrinhoResultsAvailable(mapResposta);
            }
        }

    }


    private void comunicaComOServer(){
        Log.d(TAG, "comunicaComOServer: enters");

        Activity activity = mActivity.get();
        if (activity == null
                || activity.isFinishing()
                || activity.isDestroyed()
                ) {
            return;
        }

//        Resources res = activity.getResources();
        try {
            socketGerente = new Socket(MainViewActivity.ipTexto, Integer.valueOf(MainViewActivity.portaTexto));

            out = new ObjectOutputStream(socketGerente.getOutputStream()); //mandar mensagens
            in = new ObjectInputStream(socketGerente.getInputStream()); //receber msgs

            // Load CAs from an InputStream
            // (could be from a resource or ByteArrayInputStream or ...)
//            CertificateFactory cf = CertificateFactory.getInstance("X.509");
//            // From https://www.washington.edu/itconnect/security/ca/load-der.crt
//            AssetManager am = activity.getAssets();
//
//            InputStream caInput = new BufferedInputStream(am.open("bartender.crt"));
//            Certificate ca;
//            try {
//                ca = cf.generateCertificate(caInput);
//                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
//            } finally {
//                caInput.close();
//            }
//
//            // Create a KeyStore containing our trusted CAs
//            String keyStoreType = KeyStore.getDefaultType();
//            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//            keyStore.load(null, null);
//            keyStore.setCertificateEntry("ca", ca);
//
//// Create a TrustManager that trusts the CAs in our KeyStore
//            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//            tmf.init(keyStore);
//
//// Create an SSLContext that uses our TrustManager
//            SSLContext context = SSLContext.getInstance("TLS");
//            context.init(null, tmf.getTrustManagers(), null);
//
//// Tell the URLConnection to use a SocketFactory from our SSLContext
//            URL url = new URL("https://192.168.0.11:9000");
//            HttpsURLConnection urlConnection =
//                    (HttpsURLConnection)url.openConnection();
//            urlConnection.setSSLSocketFactory(context.getSocketFactory());
//            out = new ObjectOutputStream(urlConnection.getOutputStream());  //código gera uma exception aqui
//            out.writeObject("a");   //teste qqr pra ver se chega algo lá


        } catch (IOException e) {
            e.printStackTrace();
            Logger.getLogger(ProdutoDetalheViewActivity.class.getName()).log(Level.SEVERE, null, e);
        } //catch (CertificateException e) {
//            e.printStackTrace();
//            Logger.getLogger(ProdutoDetalheViewActivity.class.getName()).log(Level.SEVERE, null, e);
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//            Logger.getLogger(ProdutoDetalheViewActivity.class.getName()).log(Level.SEVERE, null, e);
//        } catch (KeyStoreException e) {
//            e.printStackTrace();
//            Logger.getLogger(ProdutoDetalheViewActivity.class.getName()).log(Level.SEVERE, null, e);
//        } catch (KeyManagementException e) {
//            e.printStackTrace();
//            Logger.getLogger(ProdutoDetalheViewActivity.class.getName()).log(Level.SEVERE, null, e);
//        }



        Log.d(TAG, "comunicaComOServer: ends");
    }

    private void enviarMsg(HashMap<String,Object> mapEntrada){
        Log.d(TAG, "enviarMsg: enters");

        // modelo cliente-servidor
        // enviar hash para o servidor
        try {
            out.writeObject(mapEntrada);
            Log.d(TAG, "enviarMsg: mensagem enviada "+ mapEntrada);
            out.flush();
            out.reset();
            mapResposta = (HashMap<String, Object>) in.readObject();
        } catch (IOException ex) {
            //TODO
            Log.d(TAG, "enviarMsg: erro nao tratado");
            Logger.getLogger(ProdutoDetalheViewActivity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "enviarMsg: ends");
    }



}
