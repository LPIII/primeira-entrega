package com.example.alissongaliza.bartender.Presenter;

import com.example.alissongaliza.bartender.Interfaces.CreateAccountContract;

import java.util.HashMap;

/**
 * Created by Alisson on 08/04/2018.
 */

public class CreateAccountPresenter implements CreateAccountContract.Presenter{

    private CreateAccountContract.View mView;

    public CreateAccountPresenter(CreateAccountContract.View mView) {
        this.mView = mView;
        mView.setPresenter(this);

    }

    @Override
    public void subscribe() {
        //TODO: Se inscrever num evento(observável) com o RxJava
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void createAccountClicked(HashMap<String, Object> params) {
        //TODO: mando os dados pro servidor (por onde esses dados passam antes de chegar la ainda n foi acordado)
    }
}
