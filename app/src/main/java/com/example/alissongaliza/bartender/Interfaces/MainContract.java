package com.example.alissongaliza.bartender.Interfaces;

import com.example.alissongaliza.bartender.Interfaces.BasePresenter;
import com.example.alissongaliza.bartender.Interfaces.BaseView;

import java.util.HashMap;

/**
 * Created by Alisson on 11/04/18.
 */

public interface MainContract {
    interface View extends BaseView<Presenter> {
        void respostaBusca(HashMap<String, Object> listaDeBebidas);
        void enderecoNaoExiste(String message);
    }

    interface Presenter extends BasePresenter {
        void buscarProduto(String query);
        void mudarEndereco(String endereco);

    }
}
