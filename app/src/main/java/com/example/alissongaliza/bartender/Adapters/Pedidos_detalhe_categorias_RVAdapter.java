package com.example.alissongaliza.bartender.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alissongaliza.bartender.JavaClasses.Pedido;
import com.example.alissongaliza.bartender.R;

/**
 * Created by Alisson on 15/01/2018.
 */

public class Pedidos_detalhe_categorias_RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private Pedido pedido;

    public Pedidos_detalhe_categorias_RVAdapter(Context context, Pedido pedido) {
        mContext = context;
        this.pedido = pedido;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case 0:
                View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pedidos_detalhe_infopedido, parent, false);
                return new Informacoes(view1);

            case 1:
                View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pedidos_detalhe_listaprodutos, parent, false);
                return new Produtos(view2);

            case 2:
                View view3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.pedidos_detalhe_pagamento, parent, false);
                return new Pagamento(view3);

            default:
                return new Informacoes(LayoutInflater.from(parent.getContext()).inflate(R.layout.pedidos_detalhe_infopedido, parent, false));
        }



    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Resources res = mContext.getResources();
        switch (holder.getItemViewType()){
            case 0:
                Informacoes vh1 = (Informacoes) holder;

                SpannableStringBuilder dataHora = new SpannableStringBuilder(res.getString(R.string.pedidos_detalhe_infopedido_textView_dataHora));
                dataHora.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                dataHora.append(" ").append(pedido.getData()).append(" às ").append(pedido.getHora());
                vh1.dataHora.setText(dataHora);

                SpannableStringBuilder endEntrega = new SpannableStringBuilder(res.getString(R.string.pedidos_detalhe_infopedido_textView_endEntrega));
                endEntrega.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 19, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                endEntrega.append(" ").append(pedido.getEnderecoEntrega());
                vh1.endEntrega.setText(endEntrega);

                SpannableStringBuilder nfe = new SpannableStringBuilder(res.getString(R.string.pedidos_detalhe_infopedido_textView_NFE));
                nfe.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                nfe.append(" ").append(pedido.getNfe());
                vh1.nfe.setText(nfe);
                break;

            case 1:
                Produtos vh2 = (Produtos) holder;
                vh2.produtosExpandidosRV.setAdapter(new Pedidos_detalhe_categorias_expandido_RVAdapter(mContext,pedido.getListaBebidas()));
                vh2.produtosExpandidosRV.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false));
                break;

            case 2:
                Pagamento vh3 = (Pagamento) holder;

                SpannableStringBuilder metodoPagamento = new SpannableStringBuilder(res.getString(R.string.pedidos_detalhe_pagamento_textView_metodo));
                metodoPagamento.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 21, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                metodoPagamento.append(" ").append(pedido.getMetodoDePagamento()).append(" ").append(pedido.getCartao());
                vh3.metodo.setText(metodoPagamento);

                SpannableStringBuilder totalSemDesconto = new SpannableStringBuilder(res.getString(R.string.pedidos_detalhe_pagamento_textView_totalSemDesconto));
                totalSemDesconto.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                totalSemDesconto.append(" ").append(String.format(java.util.Locale.US,"%.2f", pedido.getTotalSemDesconto()).replace('.',','));
                vh3.totalSemDesconto.setText(totalSemDesconto);

                SpannableStringBuilder frete = new SpannableStringBuilder(res.getString(R.string.pedidos_detalhe_pagamento_textView_frete));
                frete.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                frete.append(" ").append(String.format(java.util.Locale.US,"%.2f", pedido.getFrete()).replace('.',','));
                vh3.frete.setText(frete);

                SpannableStringBuilder desconto = new SpannableStringBuilder(res.getString(R.string.pedidos_detalhe_pagamento_textView_desconto));
                desconto.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 10, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                desconto.append(" ").append(String.format(java.util.Locale.US,"%.2f", pedido.getDesconto()).replace('.',','));
                vh3.desconto.setText(desconto);

                SpannableStringBuilder totalComDesconto = new SpannableStringBuilder(res.getString(R.string.pedidos_detalhe_pagamento_textView_totalComDesconto));
                totalComDesconto.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 13, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                totalComDesconto.append(" ").append(String.format(java.util.Locale.US,"%.2f", pedido.getTotalComDesconto()).replace('.',','));
                vh3.totalComDesconto.setText(totalComDesconto);

                break;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private class Informacoes extends RecyclerView.ViewHolder{

        TextView titulo;
        TextView nfe;
        TextView dataHora;
        TextView endEntrega;

        Informacoes(View itemView) {
            super(itemView);
            titulo = itemView.findViewById(R.id.pedidos_detalhe_infopedido_textView_titulo);
            nfe = itemView.findViewById(R.id.pedidos_detalhe_infopedido_textView_NFE);
            dataHora = itemView.findViewById(R.id.pedidos_detalhe_infopedido_textView_dataHora);
            endEntrega = itemView.findViewById(R.id.pedidos_detalhe_infopedido_textView_endEntrega);
        }
    }

    private class Pagamento extends RecyclerView.ViewHolder{

        TextView titulo;
        TextView metodo;
        TextView totalSemDesconto;
        TextView frete;
        TextView desconto;
        TextView totalComDesconto;

        Pagamento(View itemView) {
            super(itemView);
            titulo = itemView.findViewById(R.id.pedidos_detalhe_pagamento_textView_titulo);
            metodo = itemView.findViewById(R.id.pedidos_detalhe_pagamento_textView_metodo);
            totalSemDesconto = itemView.findViewById(R.id.pedidos_detalhe_pagamento_textView_totalSemDesconto);
            frete = itemView.findViewById(R.id.pedidos_detalhe_pagamento_textView_frete);
            desconto = itemView.findViewById(R.id.pedidos_detalhe_pagamento_textView_desconto);
            totalComDesconto = itemView.findViewById(R.id.pedidos_detalhe_pagamento_textView_totalComDesconto);
        }
    }

    private class Produtos extends RecyclerView.ViewHolder{

        TextView titulo;
        RecyclerView produtosExpandidosRV;

        Produtos(View itemView) {
            super(itemView);
            titulo = itemView.findViewById(R.id.pedidos_detalhe_listaprodutos_expandido_textView_nome);
            produtosExpandidosRV = itemView.findViewById(R.id.pedidos_detalhe_listaprodutos_RV);
        }
    }
}
