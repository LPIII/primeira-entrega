package com.example.alissongaliza.bartender.Presenter;

import com.example.alissongaliza.bartender.Interfaces.LoginContract;

import java.util.HashMap;

/**
 * Created by ci on 11/04/18.
 */

public class LoginPresenter implements LoginContract.Presenter {

    LoginContract.View view;

    public LoginPresenter(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void subscribe() {
        //RxJava
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void fazerLogin(HashMap<String, Object> params) {

    }

    @Override
    public void criarConta(HashMap<String, Object> params) {

    }

    @Override
    public void entrarGoogle() {

    }

    @Override
    public void entrarFacebook() {

    }
}
