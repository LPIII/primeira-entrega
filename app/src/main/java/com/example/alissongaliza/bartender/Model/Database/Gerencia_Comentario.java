package com.example.alissongaliza.bartender.Model.Database;

import android.content.ContentValues;
import android.provider.BaseColumns;

public class Gerencia_Comentario implements BaseColumns {
    public static final String TAB_COMENTARIO = "comentario";
    public static final String IDCOMEN_COMENTARIO = "idComen";
    public static final String PRODUTO_CODIPRODU_COMENTARIO = "produto_codiProdu";
    public static final String DATACOMEN_COMENTARIO = "dataComen";
    public static final String AVALCOMEN_COMENTARIO = "avalComen";
    public static final String COMECOMEN_COMENTARIO = "comeComen";
    public static final String NOMECOMEN_COMENTARIO = "nomeComen";
    public static final String IMAGCOMEN_COMENTARIO = "imagComen";


    public static String criaComentario() {

        String SQL = "CREATE TABLE IF NOT EXISTS" + OpenHelper.NOME_BANCO + "." + NOMECOMEN_COMENTARIO + "(" +
                IDCOMEN_COMENTARIO + " INT NOT NULL AUTO_INCREMENT," +
                PRODUTO_CODIPRODU_COMENTARIO + " CHAR(13) NOT NULL," +
                DATACOMEN_COMENTARIO + " DATETIME NOT NULL," +
                AVALCOMEN_COMENTARIO + " FLOAT(2,1) NULL," +
                COMECOMEN_COMENTARIO + " TEXT(512) NULL," +
                NOMECOMEN_COMENTARIO + " VARCHAR(45) NULL," +
                IMAGCOMEN_COMENTARIO + " TEXT(50) NULL," +
                "PRIMARY KEY (" + IDCOMEN_COMENTARIO + "," + PRODUTO_CODIPRODU_COMENTARIO + ")," +
                "UNIQUE INDEX idComen_UNIQUE (" + IDCOMEN_COMENTARIO + " ASC)," +
                "CONSTRAINT fk_comentario_produto1" +
                "FOREIGN KEY (" + PRODUTO_CODIPRODU_COMENTARIO + ")" +
                "REFERENCES" + OpenHelper.NOME_BANCO + "." + TAB_COMENTARIO + " (" + Gerencia_Produto.CODIPRODU_PRODUTO + ")" +
                "ON DELETE NO ACTION" +
                "ON UPDATE NO ACTION)" +
                "ENGINE = InnoDB;";
        return SQL;
    }

    protected ContentValues gerarContentValuescomentario(POJO_Comentario comentario) {

        ContentValues values = new ContentValues();
        values.put(IDCOMEN_COMENTARIO, comentario.getId());
        values.put(PRODUTO_CODIPRODU_COMENTARIO, comentario.getProduto_codiProdu());
        values.put(DATACOMEN_COMENTARIO, comentario.getDataComen());
        values.put(AVALCOMEN_COMENTARIO, comentario.getAvalComen());
        values.put(COMECOMEN_COMENTARIO, comentario.getComeComen());
        values.put(NOMECOMEN_COMENTARIO, comentario.getnomeClien());
        values.put(IMAGCOMEN_COMENTARIO, comentario.getImagComen());


        return values;
    }
}
